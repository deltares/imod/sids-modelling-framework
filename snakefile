import shutil
import pathlib

## Global variables
modelname= "Laura"

# Run model after writing it
run_model = True

# Set to false if you want to run the model in parallel, set the cores to the amount of cores available
run_parallel = True
cores = 4

# Discretization
cellsize = 25
zmin = -40
zmax = 3
start_time = "01-01-2000"
# Amount of timesteps will be used when using constant recharge, end time will be used when entering a file with variable recharge
end_time = "31-12-2020"
amount_of_timesteps = 20 
frequency = "MS"

# If a spin-up time is required use the below settings, set spin_up to True
# During the spin-up time a constant recharge is used which is the average of the recharge input
spin_up = False
spin_up_time = 120 # Amount of time steps in the spin up period, same resolution/frequency as the frequency defined above.

# If use variable timesteps is True, Define parameters below, timestep lengthe increases over time 
use_variable_timesteps =  False
amount_of_hours = 0 # Hours for correct simulation of flood
amount_of_days = 0 # Days for longer period after the hours that simulate flood
amount_of_months= 240 # Months after the periods of days

visualization_increment = 1 # number of increments between figures (24 = every 24th timestep will be visualized)

other_heads = False # 'Warme start' with other heads and conc input

## Flood
use_flood = False
flood_conc = 16.0 # kg/m3
flood_time = "1/1/2000 01:00:00" #month/day/year 
max_infiltration_rate = 0.72 # m/day (= 3 cm/hour)

path_flood_map = r'p:\11205176-cva-majuro-atoll\06_modelling\03_SFINCS\03_modelruns_v2_runlongerfor_GWcoupling\all_slr_0_RP_50\sfincs_map.nc'

## Observation dataset properties
# If you want to implement observations, set the following flag to True
use_observation_wells = True

# In case an observation dataset of groundwater levels and concentration is present, add the path here
# NOTE: the following columns have to be present and named accordingly: x, y, filt_depth, head, conc
obs_path = "data/2_interim/"

# Set to True when no observation dataset is present and an artifical one is required
create_obs_wells = True

# If create_obs_wells is set to True, enter variables below
grid_style = "random" # use "random" or "uniform" to indicate random or equidistant grid
number_of_obs = 15
maximum_filt_depth = -10

## Extraction wells dataset properties
# If you want to implement extractions, set the following flag to True
use_extraction_wells = False

# In case an extraction dataset is present, add the path here
# NOTE: the following columns have to be present and named accordingly: x, y, filt_depth, q (Q is in m3/d)
wells_path = "data/2_interim/"

# Option to add horizontal well, horizontal wells cannot be created automatically and a shapefile with the location needs to be added. 
# To use a horizontal well, set horizontal_wells to True and enter the parameters below Horizontal wells are possible to be used in combination with point wells, both automatically created as well as added with a file 
use_horizontal_wells = False # True or False
path_horizontal_well_location = r"data\1_external\Laura_island\horizontal_wells.shp"
depth_drain = -1.0 # in mMSL
rate = 20.0 # in m3/d

# Set to True when no extraction dataset is present and an artifical one is required
create_extraction_wells = True

# If create_extraction_wells is set to True, enter variables below
grid_style_extractions = "random" # use "random" or "uniform" to indicate random or equidistant grid
number_of_extraction = 0
maximum_filt_depth_extract = -5
maximum_q = -20.0

## Synthetic DEM properties
use_synthetic_dem = False

# if use_synthetic_dem is False, specify dem path below
dem_path = r"c:\Users\kelde_ts\data\3_projects\SIDS\sids-modelling-framework\data\1_external\Laura_island\Laura_DEM_full.tif"

# If a synthetic dem is used, define parameters below
island_width = 500
island_shape = 'boomerang' # use ellipse or boomerang
fraction_above2m = 0.3
xmax = island_width+425
xmin = 0
ymax = island_width+750
ymin = xmin
dx = 1
dy = 1
size_top_dz = 0.25 #dz size for cells above ground level

## Apply sea level rise, enter sea level in mMSL
sea_level = 0.0

## Subsurface properties
# First Holocene Layer
depth_first_holocene = -5
orientation_dip_first_holocene = 90
inclination_first_holocene = 1.5

kh_first_holocene = 50.
gradient_kh_first_holocene_layer = True
dip_direction_first_holocene = 90
gradient_first_hol = 35

kv_first_holocene = 15.0
porosity_first_holocene = 0.2

# Second Holocene Layer
depth_second_holocene = -12
orientation_dip_second_holocene = 90
inclination_second_holocene = 1.5

kh_second_holocene = 350.0
gradient_kh_second_holocene_layer = True
dip_direction_sec_holocene = 90
gradient_sec_hol = 100

kv_second_holocene = 45.0
porosity_second_holocene = 0.2

# Pleistocene layer
kh_pleistocene = 500.0
kv_pleistocene = 50.0
porosity_pleistocene = 0.3

# Properties ocean
kh_ocean = 1000.0
kv_ocean = 10.0
porosity_ocean = 0.99

# Storage properties
specific_storage_constant = 1.e-5
specific_yield_constant = 0.15
specific_yield_ocean = 0.99

## Recharge and evapotranspiration properties
varying_rch = False # If varying_rch = False, enter variables below, otherwise specify rch_path
rch_amount = 0.002
rch_amount_drymonth = 0.0
dry_months = 6

rch_path = "data/1_external/wotje/wotje_recharge_flatten.csv" # Ensure column with the recharge is called "recharge" and the unit is mm/d. Time colume in yyyy-mm-dd format.

use_evt = False
evt_max_amount = 0.00538 # m/day
root_ex_depth_number = 2.5
rch_concentration = 0.0

## General head boundary (sea) properties
resistance_ghb = 1.0
slope_density_conc = 1.25
density_ref = 1000.0
stage_ghb = sea_level
conc_ghb = 16.0

## Properties drain that simulates overland flow
resistance_drn = 1.0
# Starting concentration
starting_concentration = 16.0

# Automatic path for dem is used when synthetic dem is True
dem_path_input = dem_path
if use_synthetic_dem == True:
    dem_path = f"data/2_interim/{modelname}/dem.tif"
    dem_path_input = "data/2_interim/"

# Automatic path for observation wells is used when create obs is True
obs_path_input = obs_path
if create_obs_wells == True:
    obs_path = f"data/2_interim/{modelname}/obs_wells.csv"
    obs_path_input = "data/2_interim/"

# Automatic path for extraction wells is used when create obs is True
wells_path_input = wells_path
if create_extraction_wells == True:
    wells_path = f"data/2_interim/{modelname}/wells.csv"
    wells_path_input = "data/2_interim/"

# Automatic path for flood
flood_path_input = "data/2_interim"
if use_flood == True:
    flood_path_input = path_flood_map

# copy snakefile to interim folder to save the parameter settings
pathlib.Path(f"data/2_interim/{modelname}").mkdir(
    exist_ok=True, parents=True
)
shutil.copy("snakefile", f"data/2_interim/{modelname}/snakefile") 


## Overall rule to use to create, run, and visualize the model output
rule run_and_visualize:
    input:
        f"data/5_visualization/{modelname}/total_mass/total_amount_fresh_water_0.15gl.png",
        f"data/5_visualization/{modelname}/conc_crossections/map.png",
        f"data/5_visualization/{modelname}/timeseries/map.png"

# Defined rules
rule make_island_shape:
    input:
        "src/1_prepare/island_shape.py",
        dem_path = dem_path_input,
    group: "group1"
    params:
        island_width = island_width,
        island_shape = island_shape,
        fraction_above2m = fraction_above2m,
        xmax = xmax,
        xmin = xmin,
        ymax = ymax,
        ymin = ymin,
        dx = dx,
        dy = dy,
        zmax = zmax,
        zmin = zmin,
        use_synthetic_dem = use_synthetic_dem,
        cellsize = cellsize,
        use_flood = use_flood,
    output:
        path_dem = f"data/2_interim/{modelname}/dem.tif",
        path_cross_section = f"data/2_interim/{modelname}/cross_section.shp",
    script:
        "src/1_prepare/island_shape.py"

rule make_template:
    input:
        "src/1_prepare/template.py",
        # External data
        path_area = dem_path,
    group: "group1"
    params:
        cellsize=cellsize,
        zmin=zmin,
        zmax=zmax,
        size_top_dz=size_top_dz,
    output:
        path_template = f"data/2_interim/{modelname}/template.nc",
        path_template_2d = f"data/2_interim/{modelname}/template_2d.nc",
    script:
        "src/1_prepare/template.py"

rule make_subsoil:
    input:
        "src/1_prepare/subsoil.py",
        # External data
        path_dem = dem_path,
        # Interim data
        path_template = f"data/2_interim/{modelname}/template.nc",
        path_template_2d = f"data/2_interim/{modelname}/template_2d.nc",
    group: "group1"
    params:
        zmin=zmin,
        cellsize = cellsize,
        # First holocene layer
        depth_first_holocene = depth_first_holocene,
        orientation_dip_first_holocene = orientation_dip_first_holocene,
        inclination_first_holocene = inclination_first_holocene,
        kh_first_holocene = kh_first_holocene,
        kv_first_holocene = kv_first_holocene,
        porosity_first_holocene = porosity_first_holocene,
        gradient_kh_first_holocene_layer = gradient_kh_first_holocene_layer,
        dip_direction_first_holocene = dip_direction_first_holocene,
        gradient_first_hol = gradient_first_hol,
        # Second Holocene Layer
        depth_second_holocene = depth_second_holocene,
        orientation_dip_second_holocene = orientation_dip_second_holocene,
        inclination_second_holocene = inclination_second_holocene,
        kh_second_holocene = kh_second_holocene,
        kv_second_holocene = kv_second_holocene,
        porosity_second_holocene = porosity_second_holocene,
        gradient_kh_second_holocene_layer = gradient_kh_second_holocene_layer,
        dip_direction_sec_holocene = dip_direction_sec_holocene,
        gradient_sec_hol = gradient_sec_hol,
        # Pleistocene layer
        kh_pleistocene = kh_pleistocene,
        kv_pleistocene = kv_pleistocene,
        porosity_pleistocene = porosity_pleistocene,
        # Properties ocean
        kh_ocean = kh_ocean, 
        kv_ocean = kv_ocean, 
        porosity_ocean = porosity_ocean,
        other_heads = other_heads, 
        sea_level = sea_level,
    output:
        path_dem_nc = f"data/2_interim/{modelname}/dem.nc",
        path_water_mask = f"data/2_interim/{modelname}/water_mask.nc",
        path_formation_depths = f"data/2_interim/{modelname}/formation_depths.nc",
        path_conductivity = f"data/2_interim/{modelname}/conductivity.nc",
        path_starting_heads = f"data/2_interim/{modelname}/shd.nc",
    script:
        "src/1_prepare/subsoil.py"

rule make_flood:
    input:
        "src/1_prepare/flood.py",
        # Interim data
        path_template = f"data/2_interim/{modelname}/template.nc",
        path_template_2d = f"data/2_interim/{modelname}/template_2d.nc",
        path_water_mask = f"data/2_interim/{modelname}/water_mask.nc",
        path_flood_map = flood_path_input,
        path_subsoil = f"data/2_interim/{modelname}/conductivity.nc",
    params:
        use_flood = use_flood,
        # Discretization
        start_time = start_time, 
        amount_of_hours = amount_of_hours, 
        cellsize = cellsize,
        evt_max_amount = evt_max_amount,
        # Flood parameters
        max_infiltration_rate = max_infiltration_rate,
        flood_conc = flood_conc,
        flood_time = flood_time,
    output:
        path_flood_table = f"data/2_interim/{modelname}/flood_table_{modelname}.csv",
    script:
        "src/1_prepare/flood.py"

rule make_recharge:
    input:
        "src/1_prepare/rch.py",
        # Interim data
        path_template = f"data/2_interim/{modelname}/template.nc",
        path_template_2d = f"data/2_interim/{modelname}/template_2d.nc",
        path_water_mask = f"data/2_interim/{modelname}/water_mask.nc",
        path_flood_table = f"data/2_interim/{modelname}/flood_table_{modelname}.csv",
    group: "group1"
    params:
        # Discretization
        start_time = start_time,
        use_variable_timesteps = use_variable_timesteps,
        amount_of_timesteps = amount_of_timesteps,
        end_time=end_time,
        frequency = frequency,
        amount_of_hours = amount_of_hours,
        amount_of_days = amount_of_days,
        amount_of_months = amount_of_months,
        use_flood = use_flood,
        # Recharge parameters
        varying_rch = varying_rch,
        rch_path = rch_path,
        dry_months = dry_months,
        use_evt = use_evt,
        rch_amount = rch_amount,
        rch_amount_drymonth = rch_amount_drymonth,
        evt_max_amount = evt_max_amount,
        root_ex_depth_number = root_ex_depth_number,
    output:
        path_recharge = f"data/2_interim/{modelname}/rch.nc",
    script:
        "src/1_prepare/rch.py"


rule make_ghb:
    input:
        "src/1_prepare/ghb_sea.py",
        # Interim data
        path_template = f"data/2_interim/{modelname}/template.nc",
        path_template_2d = f"data/2_interim/{modelname}/template_2d.nc",
        path_water_mask = f"data/2_interim/{modelname}/water_mask.nc",
        path_subsoil = f"data/2_interim/{modelname}/conductivity.nc",
    group: "group1"
    params:
        resistance_ghb = resistance_ghb,
        slope_density_conc = slope_density_conc,
        density_ref = density_ref,
        stage_ghb = stage_ghb,
        conc_ghb = conc_ghb,
        sea_level = sea_level,
    output:
        path_ghb = f"data/2_interim/{modelname}/generalheadboundary.nc",
    script:
        "src/1_prepare/ghb_sea.py"

rule make_drn:
    input:
        "src/1_prepare/drn.py",
        # Interim data
        path_template = f"data/2_interim/{modelname}/template.nc",
        path_template_2d = f"data/2_interim/{modelname}/template_2d.nc",
        path_water_mask = f"data/2_interim/{modelname}/water_mask.nc",
        path_dem = f"data/2_interim/{modelname}/dem.nc",
    group: "group1"
    params:
        resistance_drn = resistance_drn,
    output:
        path_drn = f"data/2_interim/{modelname}/drn.nc",
    script:
        "src/1_prepare/drn.py"

rule make_wells:
    input:
        "src/1_prepare/wells.py",
        # External dataset in case it is available
        path_extraction = wells_path_input,
        path_dem = f"data/2_interim/{modelname}/dem.nc",
        # Interim data
        path_template = f"data/2_interim/{modelname}/template.nc",
        path_template_2d = f"data/2_interim/{modelname}/template_2d.nc",
        path_water_mask = f"data/2_interim/{modelname}/water_mask.nc",
    params:
        modelname = modelname,
        use_extraction_wells = use_extraction_wells,
        create_extraction_wells = create_extraction_wells,
        grid_style_extractions = grid_style_extractions,
        number_of_extraction = number_of_extraction,
        maximum_filt_depth_extract = maximum_filt_depth_extract,
        maximum_q = maximum_q,
        use_horizontal_wells = use_horizontal_wells,
        path_horizontal_well_location = path_horizontal_well_location,
        depth_drain = depth_drain,
        rate = rate,
    output:
        path_wells = f"data/2_interim/{modelname}/extraction_wells.csv"
    script:
        "src/1_prepare/wells.py"

rule make_model:
    input:
        "src/2_build/build_model.py",
        # Interim data
        path_drainage = f"data/2_interim/{modelname}/drn.nc",
        path_ghb = f"data/2_interim/{modelname}/generalheadboundary.nc",
        path_subsoil = f"data/2_interim/{modelname}/conductivity.nc",
        path_recharge = f"data/2_interim/{modelname}/rch.nc",
        path_template = f"data/2_interim/{modelname}/template.nc",
        path_template_2d = f"data/2_interim/{modelname}/template_2d.nc",
        path_water_mask = f"data/2_interim/{modelname}/water_mask.nc",
        path_shd = f"data/2_interim/{modelname}/shd.nc",
        path_dem = f"data/2_interim/{modelname}/dem.nc",
        path_flood_table = f"data/2_interim/{modelname}/flood_table_{modelname}.csv",
        path_wells = f"data/2_interim/{modelname}/extraction_wells.csv",
    params:
        modelname = modelname,
        use_evt = use_evt,
        use_flood = use_flood,
        specific_storage_constant = specific_storage_constant,
        specific_yield_constant = specific_yield_constant,
        specific_yield_ocean = specific_yield_ocean,
        starting_concentration = starting_concentration,
        rch_concentration = rch_concentration,
        run_model = run_model,
        run_parallel = run_parallel,
        cores = cores,
        other_heads = other_heads,
        use_wells = use_extraction_wells,
        horizontal_wells = use_horizontal_wells,
        spin_up = spin_up,
        spin_up_time = spin_up_time,
        frequency = frequency,
    output:
        runfile = f"data/3_input/{modelname}/{modelname}.run"
    script:
        "src/2_build/build_model.py"

rule make_observations:
    input:
        "src/1_prepare/create_obs_wells.py",
        # External dataset in case it is available
        path_obs = obs_path_input,
        # Interim data
        path_template = f"data/2_interim/{modelname}/template.nc",
        path_water_mask = f"data/2_interim/{modelname}/water_mask.nc",
    params:
        modelname = modelname,
        use_observation_wells = use_observation_wells,
        create_obs_wells = create_obs_wells,
        grid_style = grid_style,
        number_of_obs = number_of_obs,
        maximum_filt_depth = maximum_filt_depth,
    output:
        path_obs_wells = f"data/2_interim/{modelname}/observation_wells.csv"
    script:
        "src/1_prepare/create_obs_wells.py"

rule get_cross_section:
    input:
        "src/5_visualize/cross_section.py",
        # External data
        path_cross_section_shape = f"data/2_interim/{modelname}/cross_section.shp",
        # Interim data
        path_template = f"data/2_interim/{modelname}/template.nc",
        path_water_mask = f"data/2_interim/{modelname}/water_mask.nc",
        path_dem = f"data/2_interim/{modelname}/dem.nc",
        path_subsoil = f"data/2_interim/{modelname}/conductivity.nc",
        path_formation_depth = f"data/2_interim/{modelname}/formation_depths.nc",
        runfile = f"data/3_input/{modelname}/{modelname}.run",
    params:
        modelname = modelname,
        zmin=zmin,
        visualization_increment = visualization_increment,
    output:
        f"data/5_visualization/{modelname}/conc_crossections/map.png"
    script:
        "src/5_visualize/cross_section.py"

rule get_fresh_water_volume:
    input:
        "src/5_visualize/freshwater_volume.py",
        # Interim data
        path_template = f"data/2_interim/{modelname}/template.nc",
        path_template_2d = f"data/2_interim/{modelname}/template_2d.nc",
        path_subsoil = f"data/2_interim/{modelname}/conductivity.nc",
        runfile = f"data/3_input/{modelname}/{modelname}.run",
    params:
        modelname = modelname,
    output:
        f"data/5_visualization/{modelname}/total_mass/total_amount_fresh_water_0.15gl.png"
    script:
        "src/5_visualize/freshwater_volume.py"

rule make_time_series:
    input:
        "src/5_visualize/time_series.py",
        runfile = f"data/3_input/{modelname}/{modelname}.run",
        # Interim data
        path_template = f"data/2_interim/{modelname}/template.nc",
        path_template_2d = f"data/2_interim/{modelname}/template_2d.nc",
        path_obs_wells = f"data/2_interim/{modelname}/observation_wells.csv",
        path_ext_wells = f"data/2_interim/{modelname}/extraction_wells.csv",
        path_dem = f"data/2_interim/{modelname}/dem.nc",
    params:
        modelname = modelname,
        use_extraction_wells = use_extraction_wells,
    output:
        f"data/5_visualization/{modelname}/timeseries/map.png"
    script:
        "src/5_visualize/time_series.py"
