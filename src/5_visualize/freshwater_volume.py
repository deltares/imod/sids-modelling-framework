import pathlib

import geopandas as gpd
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import xarray as xr
import imod

# Paths
path_subsoil = snakemake.input.path_subsoil
path_template = snakemake.input.path_template
path_template_2d = snakemake.input.path_template_2d

## params
params = snakemake.params
modelname = params["modelname"]

# Open template
like = xr.open_dataset(path_template)["template"]
like_2d = xr.open_dataset(path_template_2d)["template"]
dx, xmin, xmax, dy, ymin, ymax = imod.util.spatial_reference(like)
zmin = like.zbot.min()
zmax = like.ztop.max()
dz = (like.dz * -1)

# Open chloride
conc = imod.idf.open(f"data/4_output/{modelname}/conc/conc_*.idf")
conc = conc.assign_coords(dz=('layer', like.dz.values))

# Open porosity
# conductivity
conductivity = xr.open_dataset(path_subsoil)
porosity = conductivity["porosity"].swap_dims({"z": "layer"})

# Create folder to save to
pathlib.Path(f"data/5_visualization/{modelname}/total_mass").mkdir(
    exist_ok=True, parents=True
)

# Get mass loading over time
data = conc*dx*(dy *-1)*(conc.dz * -1) * porosity

# Sum all mass
som = data.sum(dim=["layer","x","y"])
som_tabel = som.to_dataframe(name = 'kgzout')
som_tabel.to_csv(rf"data/5_visualization/{modelname}/total_mass/salt_mass.csv")

# Plotting
fig, ax = plt.subplots(figsize=[10,8])
som.plot.line("b", ax=ax)
ax.set_title(f"Total amount of salt (kg)")
plt.tight_layout()

# Saving figures
fig.savefig(
    f"data/5_visualization/{modelname}/total_mass/total_amount_salt.png",
    dpi=300,
)
fig.clf()

# Freshwater volume
# 0.15 kg/m3 freshwater

# Get mass loading over time
conc_fres = conc.where(conc <= 0.15)
conc_fres = conc_fres.where(conc_fres.isnull(), 1)
data = conc_fres*dx*(dy *-1)*(conc_fres.dz * -1) * porosity * 1000

# Sum all mass
som = data.sum(dim=["layer","x","y"])
som_tabel = som.to_dataframe(name = 'freshwater_volume')

# Plotting
fig, ax = plt.subplots(figsize=[10,8])
som.plot.line("b", ax=ax)

ax.set_title(f"Total amount freshwater (L), (concentration <= 0.15 g/L)")
plt.tight_layout()

fig.savefig(
    f"data/5_visualization/{modelname}/total_mass/total_amount_fresh_water_0.15gl.png",
    dpi=300,
)
fig.clf()

## Plottin brackish water volume
# Get mass loading over time
conc_brac = conc.where(conc >= 0.15)
conc_brac = conc_brac.where(conc_brac <= 5.0)
conc_brac = conc_brac.where(conc_brac.isnull(), 1)
data = conc_brac*dx*(dy *-1)*(conc_brac.dz * -1) * porosity * 1000

# Sum all mass
som = data.sum(dim=["layer","x","y"])
data_tabel = som.to_dataframe(name = 'brackish_volume')
som_tabel['brackish_volume'] = data_tabel['brackish_volume']
som_tabel.to_csv(rf"data/5_visualization/{modelname}/total_mass/freshwatervolume.csv")

# Plotting
fig, ax = plt.subplots(figsize=[10,8])
som.plot.line("b", ax=ax)
ax.set_title(f"Total amount brackish water (L) (concentration 0.15-5.0 g/L)")
plt.tight_layout()


fig.savefig(
    f"data/5_visualization/{modelname}/total_mass/total_amount_brackish_water_0.15-5.0gl.png",
    dpi=300,
)
fig.clf()
