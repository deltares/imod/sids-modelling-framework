import pathlib
import subprocess

import geopandas as gpd
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import xarray as xr
from matplotlib.colors import ListedColormap

import imod

# params
params = snakemake.params
modelname = params["modelname"]

# Turn interactive plotting off
plt.ioff()

# make dir
pathlib.Path(f"data/5_visualization/{modelname}/3D_animation").mkdir(
    exist_ok=True, parents=True
)

# open raster data
conc = imod.idf.open(f"data/4_output/{modelname}/conc/conc_*.idf")

fresh = conc.where(conc < 5.0)
salt = conc.where(conc > 5.0)

fresh = fresh.swap_dims({"layer": "z"})
fresh = fresh.transpose("time", "y", "x", "z")

animation = imod.visualize.GridAnimation3D(fresh, mesh_kwargs=dict(cmap="jet"))

animation.plotter.camera_position = (2, 1, 0.5)
animation.plotter.add_bounding_box()

animation.peek()
