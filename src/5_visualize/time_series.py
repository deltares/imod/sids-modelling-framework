import pathlib

import geopandas as gpd
import imod
import matplotlib.lines as mlines
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import xarray as xr

# Paths
path_template = snakemake.input.path_template
path_template_2d = snakemake.input.path_template_2d
path_obs_wells = snakemake.input.path_obs_wells
path_ext_wells = snakemake.input.path_ext_wells
path_dem = snakemake.input.path_dem

## params
params = snakemake.params
modelname = params["modelname"]
use_extraction_wells = params["use_extraction_wells"]

# Open template
like = xr.open_dataset(path_template)["template"]
like_2d = xr.open_dataset(path_template_2d)["template"]

# Open dataset to plot
ds_obs = pd.read_csv(path_obs_wells, index_col=0)

if use_extraction_wells == True:
    ds_ext = pd.read_csv(path_ext_wells)
    ds_obs = ds_obs.append(ds_ext)

ds_obs.set_index(ds_obs["id"], inplace=True)
# Create GDF for plotting purposes
gdf = gpd.GeoDataFrame(ds_obs, geometry=gpd.points_from_xy(ds_obs["x"], ds_obs["y"]))
gdf["name"] = gdf["id"]

# Open DEM
dem = xr.open_dataarray(path_dem)

# PLot observation locations
# plot map of lines
plt.axis("scaled")
fig, ax = imod.visualize.plot_map(
    dem,
    colors="terrain",
    levels=np.linspace(-10, 10, 20),
    figsize=[15, 10],
    kwargs_colorbar={"label": "gw depth (m)"},
)
gdf.plot(column="name", legend=True, cmap="Paired", ax=ax, zorder=2.5)
fig.delaxes(fig.axes[1])
ax.set_title("Location observations")
pathlib.Path(f"data/5_visualization/{modelname}/timeseries/").mkdir(
    exist_ok=True, parents=True
)
fig.savefig(f"data/5_visualization/{modelname}/timeseries/map.png", dpi=300)

for mtype in ["conc", "head"]:
    print(mtype)
    # Read modelled data
    output_ds = imod.idf.open(f"data/4_output/{modelname}/{mtype}/{mtype}_*.idf")

    # Select start and end times for plotting
    sdate = output_ds.time[0].values
    edate = output_ds.time[-1].values

    # Extract modelled data
    model_ds = imod.select.points_values(
        output_ds, x=ds_obs["x"], y=ds_obs["y"], layer=ds_obs["layer"]
    ).to_dataframe()
    model_ds = model_ds.reset_index()
    # model_ds = model_ds.rename(columns={"index": "id"})
    model_ds = model_ds.drop(columns=["x", "y"])

    ## TEMP FIX
    model_ds["id"] = "obs_"
    model_ds["index"] = (model_ds["index"] +1).astype(str)
    model_ds["id"] = model_ds["id"] + model_ds["index"]

    pathlib.Path(f"data/5_visualization/{modelname}/timeseries/{mtype}/").mkdir(
        exist_ok=True, parents=True
    )

    model_ds.to_csv(f"data/5_visualization/{modelname}/timeseries/{mtype}_timeseries.csv")

    ids = model_ds.drop_duplicates(subset="id").reset_index()
    id_list = ids["id"]

    for j in id_list:
        row = 1
        col = 1

        fig, axs = plt.subplots(
            col, row, figsize=(12, 7), sharey=False, sharex=True, squeeze=False
        )
        layer = ds_obs.loc[ds_obs["id"] == j]["layer"][0]
        depth = ds_obs.loc[ds_obs["id"] == j]["filt_depth"][0]
        print(j)
        if model_ds.loc[model_ds["id"] == j][mtype].dropna().empty == False:
            ## TODO add code to plot if measurement data is present
            # if "" in j:
            #     model_ds.loc[model_ds["id"] == j].where(model_ds.loc[model_ds["id"] == j].notnull()).plot(
            #         x="time",
            #         y="Groundwater (Mean Daily)",
            #         legend=False,
            #         ax=axs[0][0],
            #         **line_layout["head_measured(m)"],
            #     )
            model_ds.loc[model_ds["id"] == j].where(
                model_ds.loc[model_ds["id"] == j].notnull()
            ).plot(
                x="time",
                y=mtype,
                color="b",
                linestyle="solid",
                linewidth=1,
                legend=False,
                ax=axs[0][0],
            )

            title_name = j
            axs[0][0].set_title(title_name)
            axs[0][0].set_xlim([sdate, edate])
            axs[0][0].set_ylim([0., 16.])
            axs[0][0].fill_between(model_ds.time,1.5,16, color="r", alpha=0.5)

            # add text box
            textstr = "\n".join(
                (
                    r"$\mathrm{Layer:}%d$" % (layer,),
                    r"$\mathrm{Depth(mMSL):}%.2f$" % (depth,),
                    r"Red area indicates water",
                    r"with a concentration of 1.5 g/L",
                )
            )

            # these are matplotlib.patch.Patch properties
            props = dict(boxstyle="round", facecolor="wheat", alpha=0.5)

            # place a text box in upper left in axes coords
            axs[0][0].text(
                1.05,
                0.98,
                textstr,
                transform=axs[0][0].transAxes,
                fontsize=12,
                verticalalignment="top",
                bbox=props,
            )

            fig.tight_layout()
            if mtype == "head":
                fig.suptitle(f"Modelled heads (mMSL)")
                l = mlines.Line2D([], [], color="blue", label="Modelled head")
            if mtype == "conc":
                fig.suptitle(f"Modelled chloride concentration (g/L)")
                l = mlines.Line2D([], [], color="blue", label="Modelled concentration")
            fig.tight_layout()
            fig.subplots_adjust(top=0.90)
            fig.legend(handles=[l], loc="upper right")
            fig.savefig(
                f"data/5_visualization/{modelname}/timeseries/{mtype}/modelled_obs_{j}.png",
                dpi=300,
            )
            plt.close()
