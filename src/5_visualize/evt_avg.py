import imod
import xarray as xr

path_evt = "data/4_output/boomerang_sealvl_25/bdgevt/*.idf"

evt = imod.idf.open(path_evt).compute()

evt_mean = evt.mean()
evt_max = evt.max()
evt_min = evt.min()