import pathlib
import subprocess

import geopandas as gpd
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import xarray as xr
from matplotlib.colors import ListedColormap

import imod


# Paths
path_cross_section_shape = snakemake.input.path_cross_section_shape
path_template = snakemake.input.path_template
path_water_mask = snakemake.input.path_water_mask
path_dem = snakemake.input.path_dem
path_subsoil = snakemake.input.path_subsoil
path_formation_depth = snakemake.input.path_formation_depth

# params
params = snakemake.params
modelname = params["modelname"]
zmin = params["zmin"]
visualization_increment = params["visualization_increment"]

# Turn interactive plotting off
plt.ioff()

# make dir
pathlib.Path(f"data/5_visualization/{modelname}/conc_crossections").mkdir(
    exist_ok=True, parents=True
)

# formation layers and bathymetry
f_depth = xr.open_dataarray(path_formation_depth)
water_mask = xr.open_dataset(path_water_mask)["is_sea3d"].swap_dims({"z":"layer"})
wm_upper = imod.select.upper_active_layer(water_mask, is_ibound=False)
bathy = wm_upper.where(wm_upper.isnull(), wm_upper["ztop"])
bathy = bathy.rename("bathymetry")
bathy = bathy.assign_coords({"layer": "bathymetry"})
bathy = bathy.expand_dims("layer")
formations = xr.concat([f_depth, bathy], dim="layer")

# Open template
like = xr.open_dataset(path_template)["template"]
like = like.swap_dims({"z": "layer"})

# open raster data
conc = imod.idf.open(f"data/4_output/{modelname}/conc/conc_*.idf")
conc = conc.isel(time=range(0, len(conc), visualization_increment))
conc = conc.to_dataset(name="chloride")
conc = conc.assign_coords(top=like["ztop"])
conc = conc.assign_coords(bottom=like["zbot"])

_, xmin, xmax, _, ymin, ymax = imod.util.spatial_reference(conc)

# open shapefile
gdf = gpd.read_file(path_cross_section_shape)
linestrings = [ls for ls in gdf.geometry]
linenames = [ls for ls in gdf.name]

# Open DEM
dem = xr.open_dataarray(path_dem)

# plot map of lines
plt.axis("scaled")
fig, ax = imod.visualize.plot_map(
    dem,
    colors="terrain",
    levels=np.linspace(-10, 10, 20),
    figsize=[15, 10],
    kwargs_colorbar={"label": "gw depth (m)"},
)
gdf.plot(column="name", legend=True, cmap="Paired", ax=ax)
fig.delaxes(fig.axes[1])
ax.set_title("Location cross sections")
fig.savefig(f"data/5_visualization/{modelname}/conc_crossections/map.png", dpi=300)

# Get sections
sections = [
    imod.select.cross_section_linestring(conc, ls) for ls in linestrings
]

aq_sections = [
    imod.select.cross_section_linestring(formations, ls).compute() for ls in linestrings
]

for species in conc.data_vars:
    for i, (cross_section, aq) in enumerate(zip(sections, aq_sections)):
        print(linenames[i])
        cross_section = cross_section[species]
        cross_section = cross_section.where(cross_section != cross_section.min())

        cross_section = cross_section.where(~(cross_section < 0.0), other=0.0)
        cross_section = cross_section.where(~(cross_section > 15.9), other=15.9)

        cross_section = cross_section.compute()

        pathlib.Path(
            f"data/5_visualization/{modelname}/conc_crossections/species{species}_{linenames[i]}"
        ).mkdir(exist_ok=True, parents=True)

        fig, ax = plt.subplots()
        kwargs_aq = {"hatch": "/", "edgecolor": "k", "facecolor": "none"}
        levels=np.array([0, 0.15, 0.5, 1, 2, 3, 5, 7.5, 10, 16])
        cmap = "jet"

        for j, time in enumerate(cross_section.time.values):
            print(j, time)
            s = cross_section.sel(time=time)
            s = s.rename("chloride (g/L)")
                
            if j == 0:
                add_colorbar=True
            else:
                add_colorbar=False

            fig, ax = imod.visualize.cross_section(
                s,
                colors=cmap,
                levels=levels,
                kwargs_colorbar={
                    "label": "Chloride",
                    "whiten_triangles": False,
                },
            )
            for lines in aq.layer:
                to_plot = aq.sel(layer=lines)
                if lines != "bathymetry":
                    to_plot = to_plot.where(to_plot < aq.sel(layer="bathymetry"))
                    to_plot.plot(color="w", linestyle='dashed', ax=ax)
                else:
                    to_plot.plot(color="k", ax=ax)

            ax.set_ylim(bottom=zmin, top=10.0)

            datestring = pd.to_datetime(time).strftime("%Y%m%d")
            title = (
                f"Section {linenames[i]}, {pd.to_datetime(time).strftime('%Y-%m-%d, %H:%M%S')}"
            )
            ax.set_title(title)
            plt.tight_layout()
            fig.savefig(
                f"data/5_visualization/{modelname}/conc_crossections/species{species}_{linenames[i]}/{j:03d}.png",
                dpi=300,
            )
            ax.clear()
            fig.clf()
        area_name =linenames[i].replace(" ", "_")
        with imod.util.cd(f"data/5_visualization/{modelname}/conc_crossections/species{species}_{linenames[i]}"):
            subprocess.call(f'"C:/Program Files/ffmpeg/bin/ffmpeg.exe" -framerate 10 -i %03d.png {modelname}_{area_name}_species{species}.webm -y"')


