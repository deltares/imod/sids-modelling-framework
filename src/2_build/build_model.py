import geopandas as gpd
import numpy as np
import pandas as pd
import xarray as xr
import time
import subprocess
import shutil
import imod
import datetime

## Paths
# Input
path_wells = snakemake.input.path_wells
path_drainage = snakemake.input.path_drainage
path_ghb = snakemake.input.path_ghb
path_subsoil = snakemake.input.path_subsoil
path_recharge = snakemake.input.path_recharge
path_template = snakemake.input.path_template
path_template_2d = snakemake.input.path_template_2d
path_water_mask = snakemake.input.path_water_mask
path_shd = snakemake.input.path_shd
path_dem = snakemake.input.path_dem
path_flood_table = snakemake.input.path_flood_table

## Params
params = snakemake.params
use_evt = params["use_evt"]
use_wells = params["use_wells"]
modelname = params["modelname"]
specific_storage_constant = params["specific_storage_constant"]
specific_yield_constant = params["specific_yield_constant"]
specific_yield_ocean = params["specific_yield_ocean"]
starting_concentration = params["starting_concentration"]
rch_concentration = params["rch_concentration"]
run_model = params["run_model"]
run_parallel = params["run_parallel"]
cores = params["cores"]
other_heads = params["other_heads"]
use_flood = params["use_flood"]
horizontal_wells = params["horizontal_wells"]
frequency = params["frequency"]
spin_up = params["spin_up"]
spin_up_time = params["spin_up_time"]

## Open data
# Open templates
like = xr.open_dataset(path_template)["template"]
like_2d = xr.open_dataset(path_template_2d)["template"]
dx, xmin, xmax, dy, ymin, ymax = imod.util.spatial_reference(like)
zmin = like.zbot.min()
zmax = like.ztop.max()
dz = like.dz

# Open Watermask
is_sea = xr.open_dataset(path_water_mask)["is_sea3d"]

# open dem
dem = xr.open_dataarray(path_dem)

# Drainage
drainage = xr.open_dataset(path_drainage)
drn_cond = drainage["drn_cond"]
drn_elev = drainage["drn_depth"]

# General Head Boundary
ghb = xr.open_dataset(path_ghb)

# conductivity
conductivity = xr.open_dataset(path_subsoil)
kh = conductivity["kh"]
kv = conductivity["kv"]
porosity = conductivity["porosity"]

# Ibound
bnd = like.where(kh.isnull(), 1.0)
bnd = bnd.fillna(0.0)
icbund = bnd.copy()

# Set top and bottom
layer = like.layer.values
bottom = bnd["zbot"].assign_coords(layer=("z", layer))
top = like.ztop.max().values

# Starting head & concentration
if other_heads == False:
    starting_head = xr.open_dataarray(path_shd)
    sconc = like.where(kh.isnull(), starting_concentration)
else:
    starting_head = imod.idf.open(f"{path_starting_head}/head/head_*.idf").squeeze(
        drop=True
    )
    sconc = imod.idf.open(f"{path_starting_head}/conc/conc_*.IDF").squeeze(drop=True)
    sconc = sconc.where(sconc > 0.0, 0.0)

# Setting storange criteria
specific_storage = like.where(kh.isnull(), specific_storage_constant)
specific_yield = like.fillna(specific_yield_constant).where(is_sea.notnull())
specific_yield = specific_yield.fillna(specific_yield_ocean).where(kh.notnull())

# Open recharge and evt package components
rch = xr.open_dataset(path_recharge)
rch = rch.load()
recharge_rate = rch["rch"]

if use_evt == True:
    evt_avg = rch["evt_avg"]
    root_ex_depth = rch["root_ex_depth"]
    surface = dem.where(recharge_rate != 0.0)
    surface = surface.transpose("time","y", "x")

# Open flood components
flood_table = pd.read_csv(path_flood_table)
flood_table.columns = [
    "number",
    "y",
    "x",
    "layer",
    "id",
    "flood_water_height",
    "rate",
    "time",
    "concentration",
]

if use_wells == True:
    wells = pd.read_csv(path_wells)
    wells["time"] = rch.time.values[1]

if horizontal_wells == True:
    wells_horizontal = pd.read_csv(f"data/2_interim/{modelname}/horizontal_wells.csv")
    wells_horizontal["time"] = rch.time.values[1]

# Getting endtime
endtime = rch.time.values[-1]

# Set spin up time
if spin_up:
    rch_avg = rch["rch"].isel(time=0, drop=True)
    dates = pd.date_range(
        end=rch["time"].values[1], periods=spin_up_time + 1, freq="YS", closed="left"
    ).to_series()
    dates = pd.concat([dates[[0]] - pd.to_timedelta("1s"), dates])
    rch_rate = xr.DataArray(1.0, {"time": dates}, ["time"]) * rch_avg
    recharge_rate = xr.concat(
        [rch_rate, rch["rch"].isel(time=slice(1, None))],
        dim="time",
    )


print("writing model")
# Build the model
m = imod.wq.SeawatModel(modelname)
m["bas"] = imod.wq.BasicFlow(
    ibound=bnd,
    top=top,
    bottom=bottom,
    starting_head=starting_head,
    inactive_head=-9999.0,
)

m["lpf"] = imod.wq.LayerPropertyFlow(
    k_horizontal=like.copy(data=kh),
    k_vertical=like.copy(data=kv),
    layer_type=0,
    specific_storage=like.copy(data=specific_storage),
    specific_yield=like.copy(data=specific_yield),
    save_budget=True,
)
m["btn"] = imod.wq.BasicTransport(
    icbund=icbund,
    starting_concentration=sconc,
    porosity=porosity,
    inactive_concentration=-9999.0,
)


m["adv"] = imod.wq.AdvectionTVD(courant=1.0)
m["dsp"] = imod.wq.Dispersion(longitudinal=1.7)
m["vdf"] = imod.wq.VariableDensityFlow(density_concentration_slope=1.34)

m["rch"] = imod.wq.RechargeHighestActive(
    rate=recharge_rate, concentration=rch_concentration, save_budget=True
)

# Flood:
if use_flood == True:
    m["flooding_well"] = imod.wq.Well(
        id_name=flood_table["number"],
        x=flood_table["x"],
        y=flood_table["y"],
        layer=flood_table["layer"],
        rate=flood_table["rate"],  # m/d
        time=flood_table["time"].astype("datetime64[ns]"),
        concentration=flood_table["concentration"],
        save_budget=True,
    )

if use_evt == True:
    m["evt"] = imod.wq.EvapotranspirationHighestActive(
        maximum_rate=evt_avg,
        surface=surface,
        extinction_depth=root_ex_depth,
        save_budget=True,
    )

if use_wells == True:
    m["wel_extraction"] = imod.wq.Well(
        id_name=wells["id"],
        x=wells["x"],
        y=wells["y"],
        layer=wells["layer"],
        rate=wells["q"],
        # time=wells["time"],
    )

if horizontal_wells == True:
    m["wel_extraction"] = imod.wq.Well(
        id_name=wells_horizontal["id"],
        x=wells_horizontal["x"],
        y=wells_horizontal["y"],
        layer=wells_horizontal["layer"],
        rate=wells_horizontal["q"],
        # time=wells_horizontal["time"],
    )

m["olf"] = imod.wq.Drainage(
    elevation=drn_elev,
    conductance=drn_cond,
    save_budget=True,
)

m["ghb"] = imod.wq.GeneralHeadBoundary(
    head=ghb["stage"],
    conductance=ghb["cond"],
    density=ghb["density"],
    concentration=ghb["conc"],
    save_budget=True,
)

m["pksf"] = imod.wq.ParallelKrylovFlowSolver(
    max_iter=150,
    inner_iter=100,
    hclose=0.0001,
    rclose=100.0,
    relax=0.98,
    partition="rcb",
)
m["pkst"] = imod.wq.ParallelKrylovTransportSolver(
    max_iter=150, inner_iter=50, cclose=1.0e-6, partition="rcb"
)
m["oc"] = imod.wq.OutputControl(
    save_head_idf=True, save_concentration_idf=True, save_budget_idf=True
)
m.time_discretization(np.datetime64(endtime))
m["time_discretization"]["transient"] = xr.full_like(
    m["time_discretization"]["timestep_duration"], True, dtype=np.bool
)
m["time_discretization"]["transient"][0] = False

m.write(
    f"data/3_input/{modelname}",
    result_dir=f"data/4_output/{modelname}",
)
# m.visualize(f"data/5-visualization/{modelname}/input")

# Create batchfile to run model
with open("data/3_input/%s/run_model.bat" % modelname, "w") as f:
    if run_parallel == True:
        f.write(
            rf'"c:\Program Files\MPICH2\bin\mpiexec.exe" -localonly {cores} "..\..\..\bin\imod-wq_svn360_x64r.exe" "{modelname}.run"'
        )
    else:
        f.write(rf'"..\..\..\bin\imod-wq_svn360_x64r.exe" {modelname}.run')
    f.write("\n")
    f.write("timeout /t 2\n")

time.sleep(2)

if run_model == True:
    print("running model")
    with imod.util.cd(f"data/3_input/{modelname}/"):
        subprocess.call(r"run_model.bat")
