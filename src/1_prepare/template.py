import geopandas as gpd
import numpy as np
import xarray as xr

import imod

def get_extent(path_area, cellsize):
    if ".shp" in path_area:    
        gdf = gpd.read_file(path_area)
        gdf = gdf.loc[gdf.value == 1]
        extent = imod.prepare.spatial.round_extent(gdf.bounds.values[0], cellsize)
    elif ".tif" in path_area:
        dem = imod.rasterio.open(path_area)
        dx, xmin, xmax, dy, ymin, ymax = imod.util.spatial_reference(dem)
        extent = xmin + 0.5 * abs(dx), ymin + 0.5 * abs(dy), xmax - 0.5 * abs(dx), ymax - 0.5 * abs(dy)
    else:
        raise FileNotFoundError("No shapefile or DEM found to derive extent from") 
    return extent

def discretization_dz(zmax, size_top_dz):
    #determine number of top cells
    n_zcells = int(zmax/size_top_dz) 
    size_top_dz = -1.0*size_top_dz
    # make np array
    dz = np.array([size_top_dz] * n_zcells + [-0.25] * 20 + [-0.5] * 10 + [-2.0] * 5 + [-5.0] * 4)
    return dz

## Set snakemake variables
params = snakemake.params
cellsize = params["cellsize"]
zmin = params["zmin"]
zmax = params["zmax"]
size_top_dz = params["size_top_dz"]

## Paths
# Input
path_area = snakemake.input.path_area
# Output
path_template = snakemake.output.path_template
path_template_2d = snakemake.output.path_template_2d

# Open data
extent = get_extent(path_area, cellsize)
xmin, ymin, xmax, ymax = extent
zmin = zmin
zmax = zmax

# Discretization
cellsize = np.float(cellsize)
dx = cellsize
dy = -cellsize
dz = discretization_dz(zmax, size_top_dz)
layer = np.arange(1, 1 + dz.size)

# Check if discretization matches with zmin and zmax, throw error if it doesn't match up:
assert dz.sum() == (
    zmin - zmax
), "Take heed, your vertical discretization does not add up to (zmax - zmin)"

# define template
dims = ("z", "y", "x")
coords = {
    "z": zmax + dz.cumsum() - 0.5 * dz,
    "y": np.arange(ymax, ymin, dy) + 0.5 * dy,
    "x": np.arange(xmin, xmax, dx) + 0.5 * dx,
    "dz": ("z", dz),
    "layer": ("z", layer),
    "zbot": ("z", zmax + dz.cumsum()),
    "ztop": ("z", zmax + dz.cumsum() - dz),
}
nrow = coords["y"].size
ncol = coords["x"].size
nlay = coords["z"].size
like = xr.DataArray(np.full((nlay, nrow, ncol), np.nan), coords, dims)
like_2d = like.isel(z=0).drop(["z", "layer", "dz", "zbot", "ztop"])

like.name = "template"
like_2d.name = "template"
like.to_netcdf(path_template)
like_2d.to_netcdf(path_template_2d)
