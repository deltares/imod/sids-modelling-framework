import numpy as np
import pandas as pd
import xarray as xr

import imod

## Paths
# Input
path_template = snakemake.input.path_template
path_template_2d = snakemake.input.path_template_2d
path_water_mask = snakemake.input.path_water_mask
path_flood_map = snakemake.input.path_flood_map
path_subsoil = snakemake.input.path_subsoil

# Output
path_flood_table = snakemake.output.path_flood_table

## Params
params = snakemake.params
use_flood = params['use_flood']
flood_conc = params['flood_conc']
flood_time = params['flood_time']
max_infiltration_rate = params['max_infiltration_rate']

start_time = params["start_time"] 
amount_of_hours = params["amount_of_hours"] 
cellsize = params['cellsize']

evt_max_amount = params['evt_max_amount']

if use_flood == True:
    # Open templates
    like = xr.open_dataset(path_template)["template"]
    like_2d = xr.open_dataset(path_template_2d)["template"]
    dx, xmin, xmax, dy, ymin, ymax = imod.util.spatial_reference(like)
    zmin = like.zbot.min()
    zmax = like.ztop.max()
    dz = like.dz

    ### Open flood data of SFINCS  
    flood_data = xr.open_dataset(path_flood_map)
    flood_data_zsmax = flood_data['zsmax']
    flood_data_zsmax = xr.where(flood_data_zsmax ==-999, np.nan, flood_data_zsmax)
    flood_map = flood_data_zsmax - flood_data['zb']

    # Create array with correct coords of flood map:
    dims = ("time","x","y")
    coords = {
        "time": flood_map.time,
        "y": flood_data.y.values[0],
        "x": np.array([i[0] for i in flood_data.x.values]),
    }
    nrow = coords["y"].size
    ncol = coords["x"].size
    flood_map = xr.DataArray(flood_map.values, coords, dims)
    flood_map = flood_map.transpose('time', 'y', 'x')
    
    # Create array with correct coords of sea in sfincs:
    dims = ("x","y")
    coords = {
        "y": flood_data.y.values[0],
        "x": np.array([i[0] for i in flood_data.x.values]),
    }
    nrow = coords["y"].size
    ncol = coords["x"].size
    sea_sfincs = xr.DataArray(xr.where(flood_data['zb'] < 0., 1., np.nan), coords, dims)


    # Find at which moment the sfincs water height reaches steady state
    counter = 0
    for (timestep, time) in enumerate(flood_map.time):
        if timestep == 0:
            flood_map_previous = flood_map[timestep].where(sea_sfincs!=1.)
            continue
        else:
            flood_map_timestep = flood_map[timestep].where(sea_sfincs!=1.)
            diff = abs(flood_map_timestep - flood_map_previous).mean().item()
            
            if (diff < 0.04) & (counter==0):
                # First timestep with low diff
                counter = 1
            elif (diff>=0.04) & (counter==1):
                # Reset counter because the low diff was only temporal
                counter = 0
            elif (diff<0.04) & (counter==1):
                # Two subsequent timesteps with low diff
                returnflow_stop = timestep - 1 #Steady state at the previous timestep
                print(f'Steady state in sfincs flood height after {returnflow_stop} hours')
                break
            flood_map_previous = flood_map_timestep
    
    flood_map_coarse = imod.prepare.Regridder(method='mean').regrid(flood_map, like=like_2d)

    # Find upper active model layer, where the flood will be applied as a Well
    conductivity = xr.open_dataset(path_subsoil)
    kh = conductivity["kh"].swap_dims({'z': 'layer'})
    upper_active_layer = imod.select.upper_active_layer(kh, is_ibound=False)


    # keep only the locations on land:
    water_mask = xr.open_dataset(path_water_mask)['is_sea2d']
    upper_active_layer = upper_active_layer.where(water_mask.isnull())
    flood_table = upper_active_layer.to_dataframe(name='upp_act_layer')
    flood_table = flood_table.drop('layer', axis=1)
    flood_table = flood_table.rename(columns={'upp_act_layer': 'layer'})
    flood_table = flood_table.reset_index()
    flood_table = flood_table.dropna()
    flood_table['id'] = 'flood'

    # make df for well with all timesteps,
    # where one timestep contains the flooding, the rest has rate = 0,

    # First timestep:
    timedelta = np.timedelta64(1, "s")  # 1 second duration for initial steady-state
    flood_table_total = flood_table.copy()
    flood_table_total['flood_water_height'] = 0. 
    flood_table_total['rate'] = 0.
    flood_table_total['time'] = pd.to_datetime(start_time) - timedelta
    flood_table_total['concentration'] = 0.


    times = pd.date_range(start_time,periods=amount_of_hours,freq='H')
    flood_times = pd.date_range(start=flood_time, periods=amount_of_hours,freq='H') # these times are mainly meant to define the start of the flood

    
    # rest of timesteps:
    for time in times:
        #print(time)
        flood_table_timestep = flood_table.copy()

        if time in flood_times:
            timestep = flood_times.get_loc(time) 

            if timestep < (returnflow_stop): 
                # These are the timesteps where the flood is still rising, 
                # or the water is flowing back towards the ocean

                # Tim and I decided that we skip these timesteps for infiltration,
                # assuming infiltration only starts when the flood stops flowing back to
                # the ocean.
                
                # create table of flood data:
                flood_height_timestep = flood_map_coarse[timestep].to_dataframe(name='flood_water_height')
                flood_height_timestep = flood_height_timestep.reset_index()
                flood_height_timestep = flood_height_timestep.fillna(0.)
                flood_table_timestep['flood_water_height'] = flood_height_timestep['flood_water_height']
                
                flood_table_timestep['rate'] = 0.
                """
                # Calculate the infiltration rate
                # this is height * area, capped at max. infiltration rate
                flood_table_timestep['rate'] = np.minimum(
                    (flood_table_timestep.flood_water_height * cellsize * cellsize * 24), 
                    (max_infiltration_rate * cellsize * cellsize))
                """
            else:
                # These are the timesteps where the water stopped flowing back to the ocean
                # In these timesteps, the infiltrated water will be subtracted from the water height
                # Slowly reducing until 0. 

                # First calculate the new height, 
                # by subtracting the infiltration rate of the previous timestep 
                # and subtracting an evaporation rate
                flood_table_timestep['flood_water_height'] = np.maximum(
                    (flood_table_timestep_previous['flood_water_height'] 
                        - (flood_table_timestep_previous['rate']/cellsize/cellsize/24)
                        - (evt_max_amount/cellsize/cellsize/24)),
                    0.)

                # Then calculate the new rate:
                # this is the maximum infiltration rate, 
                # except when the flood volume that is left is less than this maximum rate
                flood_table_timestep['rate'] = np.minimum(
                    (flood_table_timestep.flood_water_height * cellsize * cellsize * 24), 
                    (max_infiltration_rate * cellsize * cellsize))

            
            flood_table_timestep['time'] =  time.strftime("%Y-%m-%d %H:%M:%S") #pd.to_datetime(time)
            flood_table_timestep['concentration'] = flood_conc

            flood_table_timestep_previous = flood_table_timestep.copy()

            if len(flood_table_timestep[flood_table_timestep.flood_water_height>0.]) == 0.:
                # Quit writing the times if all flood water is gone (flood height = 0. everywhere)
                flood_table_timestep['flood_water_height'] = 0.
                flood_table_timestep['rate'] = 0.
                flood_table_timestep['time'] =  time.strftime("%Y-%m-%d %H:%M:%S") #pd.to_datetime(time)
                flood_table_timestep['concentration'] = 0.
                flood_table_total = pd.concat([flood_table_total, flood_table_timestep], axis=0)
                break

        else:
            flood_table_timestep['flood_water_height'] = 0.
            flood_table_timestep['rate'] = 0.
            flood_table_timestep['time'] = time.strftime("%Y-%m-%d %H:%M:%S") #pd.to_datetime(time)
            flood_table_timestep['concentration'] = 0.

        flood_table_total = pd.concat([flood_table_total, flood_table_timestep], axis=0)

    flood_table_total = flood_table_total.drop(['z','dz', 'zbot', 'ztop'], axis=1)

if use_flood == False:
    flood_table_total = pd.DataFrame(columns = ['y','x','layer','id','flood_water_height','rate', 'time', 'concentration'])

flood_table_total.to_csv(path_flood_table)
