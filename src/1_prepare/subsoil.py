import geopandas as gpd
import numpy as np
import pandas as pd
import xarray as xr

import imod


def subsoil_layers(orientation, inclination, depth):
    # orientation in degrees
    # Inclination in m/km

    dzdx = np.cos(((orientation + 90) * np.pi) / 180) * inclination / 1000
    dzdy = np.sin(((orientation + 270) * np.pi) / 180) * inclination / 1000

    # Neem de x en y waardes uit is_sea3d
    x = is_sea.x.values
    y = is_sea.y.values
    x_array = np.array([x for _ in range(len(y))])
    y_array = np.array([y for _ in range(len(x))]).T

    # Calculate change in x direction
    x_average = np.average(x)
    change_x = dzdx * (x_array - x_average)

    # Calculate change in y direction
    y_average = np.average(y)
    change_y = dzdy * (y_array - y_average)

    # Total change
    change_total = change_x + change_y

    # New z is
    znew = change_total + depth
    znew = xr.DataArray(
        data=znew, dims=["y", "x"], coords={"x": is_sea.x.values, "y": is_sea.y.values}
    )
    return znew

def get_subsurface_characteristics(
    first_hol_depth,
    sec_hol_depth,
    val_first_hol,
    val_sec_hol,
    val_pleis,
    sea_value,
    first_hol_gradient=False,
    second_hol_gradient=False,
    dip_dir_first_holocene=None,
    gr_first_hol=None,
    dip_dir_sec_holocene=None,
    gr_sec_hol=None,
):
    if first_hol_gradient is True:
        val_first_hol = subsoil_layers(dip_dir_first_holocene, gr_first_hol, val_first_hol)
        val_first_hol = val_first_hol.where(val_first_hol >= 2.0, 2.0)
    if second_hol_gradient is True:
        val_sec_hol = subsoil_layers(dip_dir_sec_holocene, gr_sec_hol, val_sec_hol)
        val_sec_hol = val_sec_hol.where(val_sec_hol >= 2.0, 2.0)

    values_grid = like.where(
        like.zbot < first_hol_depth, val_first_hol
    )  
    values_grid = values_grid.where(
        values_grid.notnull(), val_sec_hol
    )  
    values_grid = values_grid.where(like.zbot > sec_hol_depth, val_pleis)
    values_grid = values_grid.where(is_sea3d.notnull())
    sea_values = is_sea3d.where(is_sea3d.notnull(), sea_value)
    sea_values = sea_values.where(sea_values.ztop <= sea_level)
    values_grid = values_grid.combine_first(sea_values)
    return values_grid

## Paths
# Input
path_dem = snakemake.input.path_dem
path_template = snakemake.input.path_template
path_template_2d = snakemake.input.path_template_2d


# Output
path_dem_nc = snakemake.output.path_dem_nc
path_water_mask = snakemake.output.path_water_mask
path_formation_depths = snakemake.output.path_formation_depths
path_conductivity = snakemake.output.path_conductivity
path_starting_heads = snakemake.output.path_starting_heads

## Set snakemake variables
params = snakemake.params
zmin = params["zmin"]
# Subsoil parameters
# First holocene layer
depth_first_holocene = params["depth_first_holocene"]
orientation_dip_first_holocene = params["orientation_dip_first_holocene"]
inclination_first_holocene = params["inclination_first_holocene"]
kh_first_holocene = params["kh_first_holocene"]
kv_first_holocene = params["kv_first_holocene"]
porosity_first_holocene = params["porosity_first_holocene"]
gradient_kh_first_holocene_layer = params["gradient_kh_first_holocene_layer"]
dip_direction_first_holocene = params["dip_direction_first_holocene"]
gradient_first_hol = params["gradient_first_hol"]

depth_second_holocene = params["depth_second_holocene"]
orientation_dip_second_holocene = params["orientation_dip_second_holocene"]
inclination_second_holocene = params["inclination_second_holocene"]
kh_second_holocene = params["kh_second_holocene"]
kv_second_holocene = params["kv_second_holocene"]
porosity_second_holocene = params["porosity_second_holocene"]
gradient_kh_second_holocene_layer = params["gradient_kh_second_holocene_layer"]
dip_direction_sec_holocene = params["dip_direction_sec_holocene"]
gradient_sec_hol = params["gradient_sec_hol"]

kh_pleistocene = params["kh_pleistocene"]
kv_pleistocene = params["kv_pleistocene"]
porosity_pleistocene = params["porosity_pleistocene"]

kh_ocean = params["kh_ocean"]
kv_ocean = params["kv_ocean"]
porosity_ocean = params["porosity_ocean"]

other_heads = params["other_heads"]

sea_level = params["sea_level"]

# Open templates
like = xr.open_dataset(path_template)["template"]
like_2d = xr.open_dataset(path_template_2d)["template"]
dx, xmin, xmax, dy, ymin, ymax = imod.util.spatial_reference(like)
zmin = like.zbot.min()
zmax = like.ztop.max()
dz = like.dz

# Open dem
dem = imod.rasterio.open(path_dem)
dem = imod.prepare.Regridder(method="mean").regrid(dem, like_2d)
dem = dem.where(dem > zmin, zmin)
dem.to_netcdf(path_dem_nc)

# Create watermask
is_sea = dem.where(dem < sea_level)
is_sea = is_sea.where(is_sea.isnull(), 1)
is_sea.name = "is_sea"
is_sea3d = like.where(like.ztop >= dem, 1)
is_sea3d.name = "is_sea3d"
ds = xr.Dataset()
ds["is_sea2d"] = is_sea
ds["is_sea3d"] = is_sea3d
ds.to_netcdf(path_water_mask)

# Define subsoil layers
# It is asssumed three subsoil layers are present:
# First Holocene, Second Holocene, Pleistocene
# Define the bottom of each layer
f_hol_depth = subsoil_layers(
    orientation_dip_first_holocene, inclination_first_holocene, depth_first_holocene
)
s_hol_depth = subsoil_layers(
    orientation_dip_second_holocene, inclination_second_holocene, depth_second_holocene
)
pleis_depth = like_2d.where(like_2d.notnull(), like.zbot.min())

# Save interfaces
ds_all = xr.concat([f_hol_depth, s_hol_depth], dim="layer")
ds_all = ds_all.assign_coords({"layer": ["first holocene", "second holocene"]})

ds_all.to_netcdf(path_formation_depths)

# Define kh values at correct depth
kh = get_subsurface_characteristics(
    f_hol_depth,
    s_hol_depth,
    kh_first_holocene,
    kh_second_holocene,
    kh_pleistocene,
    kh_ocean,
    gradient_kh_first_holocene_layer,
    gradient_kh_second_holocene_layer,
    dip_direction_first_holocene,
    gradient_first_hol,
    dip_direction_sec_holocene,
    gradient_sec_hol,
)
kv = get_subsurface_characteristics(
    f_hol_depth,
    s_hol_depth,
    kv_first_holocene,
    kv_second_holocene,
    kv_pleistocene,
    kv_ocean,
)
prsity = get_subsurface_characteristics(
    f_hol_depth,
    s_hol_depth,
    porosity_first_holocene,
    porosity_second_holocene,
    porosity_pleistocene,
    porosity_ocean,
)

# Save
ds = xr.Dataset()
ds["kh"] = kh
ds["kv"] = kv
ds["porosity"] = prsity
ds.to_netcdf(path_conductivity)

# Get starting heads
shd = dem.where(dem > sea_level, sea_level)
shd = like.where(kh.isnull(), shd)
shd.to_netcdf(path_starting_heads)

# if other_heads == True:
    
#     shd = (imod.idf.open(r'data\2_interim\heads\head_204912010000_l*.idf')
#             .squeeze(drop=True))

#     if params['cellsize'] != 25.0:
#         # Regrid the initial heads.
#         shd_regrid = imod.prepare.Regridder(method='mean').regrid(shd, like=like_2d)
#         bnd = like.where(kh.isnull(), 1.)
#         bnd = bnd.fillna(0.)
#         bnd = bnd.swap_dims({'z': 'layer'})

#         # SHD should be defined in active cells (bnd == 1)
#         # So find the locaties where no SHD is known, and fill with nearest value.
#         shd_missing = xr.where((bnd==1.)&(shd_regrid.isnull()), True, False)
#         shd_filled = imod.prepare.fill(shd_regrid)
#         shd = xr.where(shd_missing, shd_filled, shd_regrid)

#     shd.to_netcdf(path_starting_heads)
