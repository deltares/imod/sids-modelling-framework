import numpy as np
import xarray as xr
import geopandas as gpd
import pandas as pd
from shapely.geometry import Point, LineString
import math
from scipy.interpolate import griddata

import imod

def get_extent(path_area, cellsize):
    if ".shp" in path_area:    
        gdf = gpd.read_file(path_area)
        gdf = gdf.loc[gdf.value == 1]
        extent = imod.prepare.spatial.round_extent(gdf.bounds.values[0], cellsize)
    elif ".tif" in path_area:
        dem = imod.rasterio.open(path_area)
        dx, xmin, xmax, dy, ymin, ymax = imod.util.spatial_reference(dem)
        extent = xmin + 0.5 * abs(dx), ymin + 0.5 * abs(dy), xmax - 0.5 * abs(dx), ymax - 0.5 * abs(dy)
    else:
        raise FileNotFoundError("No shapefile or DEM found to derive extent from") 
    return extent, dem

def ellipse_island(X,Y, island_width, xmax, ymax, zmax, zmin, fraction_above2m):
    ### Create height map for ellipse-shaped island
    ## Set parameters
    interval = 0.1
    t = 0
    x_gridcenter = xmax/2
    y_gridcenter = ymax/2
        
    # Outer ellipse: coastline
    a = island_width*0.5
    b = island_width*0.85
    tot_area = a*b*math.pi
    
    # Inner ellipse/circle: 2m height line
    sub_area = fraction_above2m * tot_area
    d = 0.65 * b
    c = sub_area/(d*math.pi)
    # check_subarea = c*d*math.pi
    # print('tot area is', tot_area)
    # print('sub area is', sub_area)
    # print('check sub area is', check_subarea)
    # print('subarea is', check_subarea/tot_area*100, 'percent of tot area')

    # Ellipse for elevation bottom ocean
    out_r = xmax/2
    
    ## Get ellipse coordinates
    tot_xcoords = []
    tot_ycoords = []
    sub_xcoords = []
    sub_ycoords = []
    out_xcoords = []
    out_ycoords = []
    
    while t <= 2.01 * math.pi:
        tot_xcoords.append(x_gridcenter + a * math.cos(t))
        tot_ycoords.append(y_gridcenter + b * math.sin(t))
        sub_xcoords.append(x_gridcenter + c * math.cos(t))
        sub_ycoords.append(y_gridcenter + d * math.sin(t))
        out_xcoords.append(x_gridcenter + out_r * math.cos(t))
        out_ycoords.append(y_gridcenter + out_r * math.sin(t))
        t += interval
    
    tot_xcoords = list(map(lambda x: round(x),tot_xcoords))
    tot_ycoords = list(map(lambda x: round(x),tot_ycoords))
    sub_xcoords = list(map(lambda x: round(x),sub_xcoords))
    sub_ycoords = list(map(lambda x: round(x),sub_ycoords))
    out_xcoords = list(map(lambda x: round(x),out_xcoords))
    out_ycoords = list(map(lambda x: round(x),out_ycoords))
    
    
    ## Store coordinates in lists
    points1 = [[0]*2 for i in range(len(out_xcoords))]
    k=0
    for i, j in zip(out_xcoords, out_ycoords):
        points1[k][0]=i
        points1[k][1]=j
        k+=1
    
    points2 = [[0]*2 for i in range(len(tot_xcoords))]
    k=0
    for i, j in zip(tot_xcoords, tot_ycoords):
        points2[k][0]=i
        points2[k][1]=j
        k+=1
    
    points3 = [[0]*2 for i in range(len(sub_xcoords))]
    k=0
    for i, j in zip(sub_xcoords, sub_ycoords):
        points3[k][0]=i
        points3[k][1]=j
        k+=1
    
    points4 = [[x_gridcenter,y_gridcenter]]
    points = points1 + points2 + points3 + points4
    
    ## Store elevation in lists
    elevation1 = [zmin/6] * len(points1)
    elevation2 = [0] *len(tot_xcoords)
    elevation3 = [2] *len(sub_xcoords) 
    elevation4 = [zmax]
    elevation = elevation1 + elevation2 + elevation3 + elevation4
    
    ## Inteprolate elevation data over full grid
    interpolated_data = griddata(points, elevation, (X, Y), method='linear', fill_value= zmin)
    
    ## Create LineStrings for cross-section
    WE = LineString([Point(xmin, y_gridcenter), Point(xmax, y_gridcenter)])
    NZ = LineString([Point(x_gridcenter, ymin), Point(x_gridcenter, ymax)])
    geometry = [WE, NZ]
    
    return interpolated_data, geometry

def boomerang_island(X,Y, island_width, xmax, ymax, zmax, zmin, fraction_above2m):
    ### Create height map for boomerang/triangular shaped island
    ## Set parameters
    interval = 0.1
    t = 1/2 * math.pi
    x_gridcenter = xmax/2
    y_gridcenter = ymax/2
    
    # Outer shape/coastline
    a1 = island_width* 1/3 # x-axis inner curve
    a2 = a1+(island_width) # x-axis outer curve
    b = island_width*8/6 # y-axis both curves
    tot_curve_center = 1.5*x_gridcenter
    tot_area = (a2*b*math.pi) - (a1*b*math.pi)
    
    # Inner shape/ 2m height line
    d = 0.65*b # y-axis both curves
    sub_area = fraction_above2m * tot_area
    c2 = math.sqrt((a2*b*math.pi*0.3)/math.pi) # x-axis outer curve
    c1 = c2 - (sub_area/(math.pi*d)) # x-axis for inner curve
    sub_center = 1.1*x_gridcenter
    # check_subarea = (c2*d*math.pi)-(c1*d*math.pi)
    # print('tot area is',tot_area)
    # print('sub area is', sub_area)
    # print('check sub area is', check_subarea)
    # print('sub area is', check_subarea/tot_area*100, 'percent of tot area')
    
    # Max elevation line
    c3 = c2-c1 # x-axis curve
    e = 0.85*d # y-axis curve
    
    # Elevation bottom ocean
    out_a1 = 4/7*a1
    out_a2 = a2 + 4*a1
    out_b = b+2*a1
    out_curve_center = tot_curve_center + 2*a1
    
    
    ## Get ellipse coordinates
    tot1_xcoords = []
    tot1_ycoords = []
    tot2_xcoords = []
    tot2_ycoords = []
    sub1_xcoords = []
    sub1_ycoords = []
    sub2_xcoords = []
    sub2_ycoords = []
    top_xcoords = []
    top_ycoords = []
    out1_xcoords = []
    out1_ycoords = []
    out2_xcoords = []
    out2_ycoords = []
    
    while t <= 3/2 * math.pi:
        tot1_xcoords.append(tot_curve_center + a1 * math.cos(t))
        tot1_ycoords.append(y_gridcenter + b * math.sin(t))
        tot2_xcoords.append(tot_curve_center + a2 * math.cos(t))
        tot2_ycoords.append(y_gridcenter + b * math.sin(t))
        sub1_xcoords.append(sub_center + c1 * math.cos(t))
        sub1_ycoords.append(y_gridcenter + d * math.sin(t))
        sub2_xcoords.append(sub_center + c2 * math.cos(t))
        sub2_ycoords.append(y_gridcenter + d * math.sin(t))
        top_xcoords.append(sub_center + c3 * math.cos(t))
        top_ycoords.append(y_gridcenter + e * math.sin(t))
        out1_xcoords.append(out_curve_center + out_a1 * math.cos(t))
        out1_ycoords.append(y_gridcenter + out_b * math.sin(t))
        out2_xcoords.append(out_curve_center + out_a2 * math.cos(t))
        out2_ycoords.append(y_gridcenter + out_b * math.sin(t))
        t += interval
    
    tot1_xcoords = list(map(lambda x: round(x),tot1_xcoords))
    tot1_ycoords = list(map(lambda x: round(x),tot1_ycoords))
    tot2_xcoords = list(map(lambda x: round(x),tot2_xcoords))
    tot2_ycoords = list(map(lambda x: round(x),tot2_ycoords))
    sub1_xcoords = list(map(lambda x: round(x),sub1_xcoords))
    sub1_ycoords = list(map(lambda x: round(x),sub1_ycoords))
    sub2_xcoords = list(map(lambda x: round(x),sub2_xcoords))
    sub2_ycoords = list(map(lambda x: round(x),sub2_ycoords))
    top_xcoords = list(map(lambda x: round(x),top_xcoords))
    top_ycoords = list(map(lambda x: round(x),top_ycoords))
    out1_xcoords = list(map(lambda x: round(x),out1_xcoords))
    out1_ycoords = list(map(lambda x: round(x),out1_ycoords))
    out2_xcoords = list(map(lambda x: round(x),out2_xcoords))
    out2_ycoords = list(map(lambda x: round(x),out2_ycoords))
    
    ## Store coordinates in lists
    points1 = [[0]*2 for i in range(len(out1_xcoords))]
    k=0
    for i, j in zip(out1_xcoords, out1_ycoords):
        points1[k][0]=i
        points1[k][1]=j
        k+=1
    
    points2 = [[0]*2 for i in range(len(out2_xcoords))]
    k=0
    for i, j in zip(out2_xcoords, out2_ycoords):
        points2[k][0]=i
        points2[k][1]=j
        k+=1
    
    points3 = [[0]*2 for i in range(len(tot1_xcoords))]
    k=0
    for i, j in zip(tot1_xcoords, tot1_ycoords):
        points3[k][0]=i
        points3[k][1]=j
        k+=1
       
    points4 = [[0]*2 for i in range(len(tot2_xcoords))]
    k=0
    for i, j in zip(tot2_xcoords, tot2_ycoords):
        points4[k][0]=i
        points4[k][1]=j
        k+=1
    
    points5 = [[0]*2 for i in range(len(sub1_xcoords))]
    k=0
    for i, j in zip(sub1_xcoords, sub1_ycoords):
        points5[k][0]=i
        points5[k][1]=j
        k+=1
    
    points6 = [[0]*2 for i in range(len(sub2_xcoords))]
    k=0
    for i, j in zip(sub2_xcoords, sub2_ycoords):
        points6[k][0]=i
        points6[k][1]=j
        k+=1
    
    points7 = [[0]*2 for i in range(len(top_xcoords))]
    k=0
    for i, j in zip(top_xcoords, top_ycoords):
        points7[k][0]=i
        points7[k][1]=j
        k+=1
    #drop part of points in points7
    drop_ratio = 1/3*len(points7)
    points7 = points7[int(drop_ratio/2):-int(drop_ratio/2)]
    
    points = points1 + points2 + points3 + points4 + points5 + points6 + points7
    
    ## Store elevation in lists
    elevation1 = [zmin/6] * len(points1)
    elevation2 = [zmin] * len(points2)
    elevation3 = [0] *len(points3)
    elevation4 = [0] *len(points4) 
    elevation5 = [2] *len(points5)
    elevation6 = [2] *len(points6)
    elevation7 = [zmax]*len(points7)
    elevation = elevation1 + elevation2 + elevation3 + elevation4 + elevation5 + elevation6 + elevation7
    
    ## Inteprolate elevation data over full grid
    interpolated_data = griddata(points, elevation, (X, Y), method='linear', fill_value= zmin)
    
    ## Create LineStrings for cross-section
    WE = LineString([Point(xmin, y_gridcenter), Point(xmax, y_gridcenter)])
    NZ = LineString([Point(sub_center-c3, ymin), Point(sub_center-c3, ymax)])
    geometry = [WE, NZ]
        
    return interpolated_data, geometry

## Paths
# Input
dem_path = snakemake.input.dem_path
# Output
path_dem = snakemake.output.path_dem
path_cross_section = snakemake.output.path_cross_section

# Set snakemake variables
params = snakemake.params
island_width = params["island_width"]
island_shape = params["island_shape"]
fraction_above2m = params["fraction_above2m"]
xmin = params["xmin"]
ymin = params["ymin"]
dx = params["dx"]
dy = params["dy"]
zmax = params["zmax"]
zmin = params["zmin"]
use_synthetic_dem = params["use_synthetic_dem"]
cellsize = params["cellsize"]
use_flood = params['use_flood']

if use_synthetic_dem == True:
    # Set remaining parameters (zmax,xmax,ymax)
    zmax = zmax + 0.5
    if island_shape == 'ellipse':
        xmax = island_width+550
        ymax = island_width+750
    if island_shape == 'boomerang':
        xmax = 7/3*island_width
        ymax = 3.3*island_width

    ## Create DEM
    # Make grid
    x, y = np.arange(xmin, xmax+1, dx), np.arange(ymin, ymax+1, dy)
    X, Y = np.meshgrid(x,y)

    # Add elevation to grid
    if island_shape == 'ellipse':
        elevation, geometry = ellipse_island(X, Y, island_width, xmax, ymax, zmax, zmin, fraction_above2m)
    if island_shape == 'boomerang':
        elevation, geometry = boomerang_island(X, Y, island_width, xmax, ymax, zmax, zmin, fraction_above2m)

    # Save dem
    ds = xr.DataArray(data = elevation, coords=(y,x), dims = ['y', 'x']) 
    imod.rasterio.save(path_dem, ds)

if use_synthetic_dem == False:
    ## Create LineStrings for cross-section
    extent, dem = get_extent(dem_path, cellsize)
    xmin, ymin, xmax, ymax = extent
    xmiddle = (xmin + xmax)/2
    ymiddle = (ymin + ymax)/2
    WE = LineString([Point(xmin, ymiddle), Point(xmax, ymiddle)])
    NZ = LineString([Point(xmiddle, ymax), Point(xmiddle, ymin)])
    geometry = [WE, NZ]
    if use_flood == True:
        floodNZ = LineString([Point(xmiddle - 450, ymax), Point(xmiddle - 450, ymin)])
        geometry = [WE, NZ, floodNZ]

    imod.rasterio.save(path_dem, dem)   

## Create GeoDataFrame with cross-section
if use_flood == True:
    data = {'id': [1,2,3],
        'name': ['WE','NZ', 'floodNZ']}
else:
    data = {'id': [1,2],
            'name': ['WE','NZ']}
df = pd.DataFrame(data)
gdf = gpd.GeoDataFrame(df, geometry = geometry)
gdf.to_file(filename= path_cross_section, driver='ESRI Shapefile')
