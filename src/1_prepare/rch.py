import numpy as np
import pandas as pd
import xarray as xr

import imod


def variable_recharge(dry_months, amount_of_months, rch_amount, rch_amount_drymonth):
    wet_months = 12 - dry_months
    w = 0
    d = 0
    rch_array =  np.zeros(shape = (amount_of_months,))
    
    for i in range(amount_of_months):
        if w < wet_months:
            rch_array[i] = rch_amount
            w += 1            
        elif d < dry_months:
            rch_array[i] = rch_amount_drymonth
            d += 1            
            if d == dry_months:
                w = 0
                d = 0
                
    return rch_array

#def variable_et(dry_months, amount_of_months,rch_amount, rch_amount_drymonth):
    # wet_months = 12 - dry_months
    # w = 0
    # d = 0
    # rch_array =  np.zeros(shape = (amount_of_months,))
    
    # for i in range(amount_of_months):
    #     if w < wet_months:
    #         rch_array[i] = rch_amount
    #         w += 1            
    #     elif d < dry_months:
    #         rch_array[i] = rch_amount_drymonth
    #         d += 1            
    #         if d == dry_months:
    #             w = 0
    #             d = 0
                
    # return rch_array

## Paths
# Input
path_template = snakemake.input.path_template
path_template_2d = snakemake.input.path_template_2d
path_water_mask = snakemake.input.path_water_mask
path_flood_table = snakemake.input.path_flood_table

# Output
path_recharge = snakemake.output.path_recharge

## Params
params = snakemake.params
use_evt = params["use_evt"] 
rch_amount = params["rch_amount"] 
evt_max_amount = params["evt_max_amount"] 
root_ex_depth_number = params["root_ex_depth_number"] 
start_time = params["start_time"] 
use_variable_timesteps = params['use_variable_timesteps']
amount_of_timesteps = params['amount_of_timesteps']
frequency = params['frequency']
amount_of_hours = params["amount_of_hours"] 
amount_of_days = params["amount_of_days"] 
amount_of_months = params["amount_of_months"] 
dry_months = params["dry_months"]
rch_amount_drymonth = params["rch_amount_drymonth"]
use_flood = params['use_flood']
varying_rch = params["varying_rch"]
rch_path = params["rch_path"]
end_time = params["end_time"]

# Opening data
# Open templates
like = xr.open_dataset(path_template)["template"]
like_2d = xr.open_dataset(path_template_2d)["template"]
dx, xmin, xmax, dy, ymin, ymax = imod.util.spatial_reference(like)
zmin = like.zbot.min()
zmax = like.ztop.max()
dz = like.dz

water_mask = xr.open_dataset(path_water_mask)["is_sea2d"]

## set recharge amounts over time
# create dataframe for time
if varying_rch == False:
    if use_variable_timesteps == True:
    # First list the timesteps of 1 hour, continue with timestep of days, and then months
    # If no hours will be simulated, start the list of days with start time
    # Same if no hours & no days are simulated: start list of months with start time
        times_hours = pd.date_range(start_time,periods=amount_of_hours, freq='H')

        if amount_of_hours == 0. :
            start = start_time
        else:
            start = times_hours[-1] + np.timedelta64(1, "D")

        times_days = pd.date_range(start,periods=amount_of_days, freq='D')

        if (amount_of_days == 0.) & (amount_of_hours == 0.):
            start = start_time
        elif (amount_of_days == 0.) & (amount_of_hours != 0.):
            start = times_hours[-1] + np.timedelta64(1, "M")
        else:
            start = times_days[-1] + np.timedelta64(1, "M")

        times_months = pd.date_range(start,periods=amount_of_months, freq='MS')
        time_df = pd.DataFrame({"time": times_hours.union(times_days).union(times_months)})

        # Add recharge to time dataframe 
        if dry_months == 0:
            rch_array =  np.empty(amount_of_hours+amount_of_days+amount_of_months)
            rch_array.fill(rch_amount)
            time_df['rch'] = rch_array
        else:
            time_df['rch'] = variable_recharge(dry_months, amount_of_months,rch_amount, rch_amount_drymonth) 
    
    else:
        times = pd.date_range(start_time, periods=amount_of_timesteps, freq=frequency)
        time_df = pd.DataFrame({"time": times})

        # Add recharge to time dataframe 
        if dry_months == 0:
            rch_array =  np.empty(amount_of_timesteps)
            rch_array.fill(rch_amount)
            time_df['rch'] = rch_array
        else:
            time_df['rch'] = variable_recharge(dry_months, amount_of_timesteps,rch_amount, rch_amount_drymonth)

    # change time df into xarray
    time_array = xr.DataArray(time_df["rch"], coords={"time": time_df["time"]}, dims=["time"])
    rch = like_2d.where(water_mask.notnull(), 1) *time_array

if varying_rch == True:
    df = pd.read_csv(rch_path)
    df = df.interpolate()

    # change time df into xarray
    time_array = xr.DataArray(df["recharge"], coords={"time": pd.to_datetime(df["time"])}, dims=["time"])
    rch = like_2d.where(water_mask.notnull(), 1) *time_array

    # Selecting rch for correct start and en time and resample to right frequancy
    rch = rch.sel(time=slice(start_time, end_time))
    rch = rch.resample(time=frequency).mean()

# ensuring all nans are filled
rch = rch.fillna(0.)

# calculating average recharge
rch_avg = rch.mean("time")
timedelta = np.timedelta64(1, "s")  # 1 second duration for initial steady-state
starttime = rch.coords["time"][0] - timedelta
rch_avg = rch_avg.assign_coords(time=starttime)
rch = xr.concat([rch_avg, rch], dim="time")

# Saving dataset
ds = xr.Dataset()
ds["rch"] = rch.transpose("time","y", "x")

if use_evt == True: 
    if use_flood == True:
        # During the period with a flood, there is no evt from the subsurface 
        # (this is handled as evaporation of the flood)
        # this is updated every timestep by the location where still is ponding flood water

        # Determine the duration of the flood 
        # These are all times where the floodwater height is anywhere larger than 0
        flood_table = pd.read_csv(path_flood_table)
        flood_times = pd.unique(flood_table[flood_table.flood_water_height >0.].time)#.astype('datetime64[ns]')
        
        # Make array with evt everywhere (= situation without flood) for every timestep of RCH
        evt_noflood = like_2d.where(water_mask.notnull(), evt_max_amount)
        evt_noflood = evt_noflood.fillna(0.)
        evt = evt_noflood.expand_dims(time=rch.time)

        regridder = imod.prepare.Regridder(method='mean')

        for flood_time in flood_times:
            one_flood_time = flood_table[flood_table.time == flood_time]
            one_flood_time = one_flood_time.set_index(['y', 'x']) #make multiindex
            
            # make array of table:
            one_flood_time_array = one_flood_time.to_xarray()['flood_water_height']

            # make sure the array is the correct shape (including sea)
            one_flood_time_array = regridder.regrid(one_flood_time_array, like=water_mask)
            
            # Make array without evaporation at location of flood
            evt_flood = xr.where((water_mask.isnull()&(one_flood_time_array==0.)), evt_max_amount, 0.)
            

            # Replace the evt array with evt_flood at the moments where there is a flood
            evt = xr.where(evt.time == pd.to_datetime(flood_time), evt_flood, evt)

        root_ex_depth = like_2d.where(water_mask.notnull(), root_ex_depth_number)
    else:
        evt_avg = like_2d.where(water_mask.notnull(), evt_max_amount)
        evt = evt_avg.fillna(0.)
        root_ex_depth = like_2d.where(water_mask.notnull(), root_ex_depth_number)

    ds["evt_avg"] = evt
    ds["root_ex_depth"] = root_ex_depth

ds.to_netcdf(path_recharge)