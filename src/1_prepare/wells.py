import numpy as np
import pandas as pd
import xarray as xr
import geopandas as gpd
import matplotlib.pyplot as plt
import math

import imod

def get_wells(x_value, y_value, numb_obs, max_depth, Q_max, path_ext_wells_out, sea_is):
    df_temp = pd.DataFrame()
    df_temp["x"] = x_value
    df_temp["y"] = y_value

    gdf = gpd.GeoDataFrame(
        df_temp, geometry=gpd.points_from_xy(df_temp["x"], df_temp["y"])
    )

    # Create island polygon
    island_poly = imod.prepare.polygonize(sea_is)
    island_poly = island_poly.loc[island_poly["value"] == 2.]
    island_poly.reset_index(inplace=True)
    gdf_mask = gdf.within(island_poly.loc[0,"geometry"])

    data = gdf.loc[gdf_mask].iloc[:numb_obs]

    df = pd.DataFrame()
    df["x"] = data["x"]
    df["y"] = data["y"]
    df.reset_index(inplace=True)
    df["id"] = (df.index.values  + 1).astype(str) 
    df["id"] = "extraction_" + df["id"]

    # Get a random filterdepth between -0.1 and the entered maximum filter depth
    filt_depth = np.random.uniform(-0.1, max_depth, size=(numb_obs))
    df["filt_depth"] = filt_depth

    topish = like.ztop
    botish = like.zbot

    edges = np.empty(len(like.zbot)+1)
    edges[0] = topish.values[0]
    edges[1 : len(botish) + 1] = botish.values
    edges = np.flipud(edges)

    # Get layer in which filter is present
    filter_lay = (len(like.zbot)+1) - np.searchsorted(edges, df["filt_depth"])

    # Add it to main dataframe
    df["layer"] = filter_lay

    # Get a random filterdepth between -0.5 and the entered maximum filter depth
    q = np.random.uniform(0., Q_max, size=(numb_obs))
    # make sure extraction is negative
    q = abs(q) *-1
    df["q"] = q

    df = df[["x", "y", "id", "filt_depth", "layer", "q"]]

    # save it
    df.to_csv(path_ext_wells_out)
    return df


# Snakemake paths
# input
path_extraction = snakemake.input.path_extraction
path_water_mask = snakemake.input.path_water_mask
path_template = snakemake.input.path_template
path_template_2d = snakemake.input.path_template_2d
path_dem = snakemake.input.path_dem

# output
path_wells = snakemake.output.path_wells

# params
params = snakemake.params
modelname = params["modelname"]
use_extraction_wells = params["use_extraction_wells"]
create_extraction_wells = params["create_extraction_wells"]
grid_style_extractions = params["grid_style_extractions"]
number_of_extraction = params["number_of_extraction"]
maximum_filt_depth_extract = params["maximum_filt_depth_extract"]
maximum_q = params["maximum_q"]

# horizontal well properties
horizontal_wells = params["use_horizontal_wells"]
path_horizontal_well_location = params["path_horizontal_well_location"]
depth_drain =  params["depth_drain"]
rate = params["rate"]

# Open data
like = xr.open_dataset(path_template)["template"]
like_2d = xr.open_dataset(path_template_2d)["template"]
is_sea = xr.open_dataset(path_water_mask)["is_sea2d"]
is_sea = is_sea.fillna(2.)

# Setting boundary settings
xmin = is_sea.x.min().values
xmax = is_sea.x.max().values
ymin = is_sea.y.min().values
ymax = is_sea.y.max().values

horizontal_wells = True
if use_extraction_wells == True:
    if horizontal_wells == True:
        wel_loc_gdf = gpd.read_file(path_horizontal_well_location)
        wel_loc = imod.prepare.rasterize(wel_loc_gdf.geometry, like_2d, fill=0)

        amount_cells = wel_loc.sum()
        rate_per_cell = rate / amount_cells  # m3/d

        # keep only xy coordinates as pandas df
        da_stacked = wel_loc.stack(notnull=["x", "y"])
        df = da_stacked[da_stacked.notnull()].to_pandas()
        df = df.reset_index(level=[0, 1])
        df.columns = ["x", "y", "q"]

        # add rate
        df = df[df.q == 1.0]
        df.q = df.q * rate_per_cell.values

        layerofdrn = like.sel(z=depth_drain, method="nearest").layer.values
        df["layer"] = layerofdrn
        df.insert(0, "id", range(1, 1 + len(df)))

        df["id"] = "diepdrain_" + df["id"].astype(str)


        df = df[["x", "y", "id", "layer", "q"]]
        df.to_csv(f"data/2_interim/{modelname}/horizontal_wells.csv")

        df = pd.DataFrame()
        df.to_csv(path_wells)

    if create_extraction_wells == False:
        df = pd.read_csv(path_extraction)
        # get layer
        topish = like.ztop
        botish = like.zbot

        edges = np.empty(len(like.zbot)+1)
        edges[0] = topish.values[0]
        edges[1 : len(botish) + 1] = botish.values
        edges = np.flipud(edges)

        # Get layer in which filter is present
        filter_lay = (len(like.zbot)+1) - np.searchsorted(edges, df["filt_depth"])

        # Add it to main dataframe
        df["layer"] = filter_lay

        df_made = df[["x", "y", "id", "filt_depth", "layer", "q"]]
        df_made.to_csv(path_wells)

    if create_extraction_wells == True:
        if grid_style_extractions == "uniform":
            # create a larger dataset and mask it with the 
            i_temp = number_of_extraction * 5
            i_sqrt = math.ceil(math.sqrt(i_temp))
            list_x = np.linspace(xmin, xmax, num=i_sqrt)
            list_y = np.linspace(ymin, ymax, num=i_sqrt)

            dataxy = [(x,y) for x in list_x for y in list_y]

            df_temp = pd.DataFrame(dataxy, columns =["x", "y"])

            df_made = get_wells(df_temp["x"].values, df_temp["y"].values, number_of_extraction, maximum_filt_depth_extract, maximum_q, path_wells, is_sea)

        if grid_style_extractions == "random":
            i_temp = number_of_extraction * 100
            x = np.random.randint(xmin, xmax, size=(i_temp))
            y = np.random.randint(ymin, ymax, size=(i_temp))

            df_made = get_wells(x, y, number_of_extraction, maximum_filt_depth_extract, maximum_q, path_wells, is_sea)

    # Open DEM
    dem = xr.open_dataarray(path_dem)

    gdf = gpd.GeoDataFrame(df_made, geometry=gpd.points_from_xy(df_made["x"], df_made["y"]))
    gdf["name"] = gdf["id"].replace("_", " ", regex=True)

    # PLot observation locations
    # plot map of lines
    plt.axis("scaled")
    fig, ax = imod.visualize.plot_map(
        dem,
        colors="terrain",
        levels=np.linspace(-10, 10, 20),
        figsize=[15, 10],
        kwargs_colorbar={"label": "gw depth (m)"},
    )

    if len(gdf.columns) == 0:
        gdf.plot(column="name", legend=True, cmap="Paired", ax=ax, zorder=2.5)

    if horizontal_wells == True:
        wel_loc_gdf.plot(column="name", legend=True, cmap="Paired", ax=ax)

    fig.delaxes(fig.axes[1])
    ax.set_title("Location extraction_wells")
    fig.savefig(f"data/2_interim/{modelname}/well_location.png", dpi=300)

else:
    df = pd.DataFrame()
    df.to_csv(path_wells)


