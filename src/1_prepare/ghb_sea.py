import geopandas as gpd
import numpy as np
import pandas as pd
import xarray as xr

import imod

## Paths
# Input
path_template = snakemake.input.path_template
path_template_2d = snakemake.input.path_template_2d
path_water_mask = snakemake.input.path_water_mask
path_subsoil = snakemake.input.path_subsoil
# Output
path_ghb = snakemake.output.path_ghb

## Params
params = snakemake.params
resistance = params["resistance_ghb"]
slope_density_conc = params["slope_density_conc"]
density_ref = params["density_ref"]
stage_ghb = params["stage_ghb"]
conc_ghb = params["conc_ghb"]
sea_level = params["sea_level"]

# Opening data
# Open templates
like = xr.open_dataset(path_template)["template"]
like_2d = xr.open_dataset(path_template_2d)["template"]
dx, xmin, xmax, dy, ymin, ymax = imod.util.spatial_reference(like)
zmin = like.zbot.min()
zmax = like.ztop.max()
dz = like.dz

is_sea = xr.open_dataset(path_water_mask)["is_sea3d"]

# Create ghb components
# Find upper active model layer, where the general head boundary will be applied
conductivity = xr.open_dataset(path_subsoil)
kh = conductivity["kh"].swap_dims({'z': 'layer'})
upper_active_layer = imod.select.upper_active_layer(kh, is_ibound=False)

# keep only the locations in sea:
water_mask = xr.open_dataset(path_water_mask)['is_sea2d']
upper_active_layer = upper_active_layer.where(water_mask.notnull())

# create the area where the GHB will be present
is_ghb = xr.full_like(like, 1.).swap_dims({'z': 'layer'})
is_ghb = is_ghb.where(is_ghb.layer == upper_active_layer)
is_ghb = is_ghb.swap_dims({'layer': 'z'})

# Conductance, take ~ 1 day for stability
# Divide resistance by cell area
conductance = abs(dx * dy) / resistance
ghb_cond = xr.full_like(like, 1.0) * conductance

# set ghb's
ghb_stage = xr.full_like(like, stage_ghb)
ghb_conc = xr.full_like(like, conc_ghb)
ghb_density = ghb_conc * slope_density_conc + density_ref

# making sure GHB is only present in the right layer
ghb_cond = ghb_cond.where(is_ghb.notnull())
ghb_stage = ghb_stage.where(is_ghb.notnull())
ghb_conc =  ghb_conc.where(is_ghb.notnull())
ghb_density = ghb_density.where(is_ghb.notnull())

# asserting to ensure ghb is present in the right cells
assert ghb_cond.count() == ghb_stage.count() == ghb_conc.count() == ghb_density.count()

# Save
ds = xr.Dataset()
ds["stage"] = ghb_stage
ds["conc"] = ghb_conc
ds["cond"] = ghb_cond
ds["density"] = ghb_density
ds = ds.reindex_like(like).transpose("z","y","x")
ds.to_netcdf(path_ghb)
