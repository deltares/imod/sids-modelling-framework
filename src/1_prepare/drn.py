import numpy as np
import pandas as pd
import xarray as xr

import imod

## Paths
# Input
path_template = snakemake.input.path_template
path_template_2d = snakemake.input.path_template_2d
path_water_mask = snakemake.input.path_water_mask
path_dem = snakemake.input.path_dem
# Output
path_drn = snakemake.output.path_drn

## Params
params = snakemake.params
resistance = params["resistance_drn"]

# Opening data
# Open templates
like = xr.open_dataset(path_template)["template"]
like_2d = xr.open_dataset(path_template_2d)["template"]
dx, xmin, xmax, dy, ymin, ymax = imod.util.spatial_reference(like)
zmin = like.zbot.min()
zmax = like.ztop.max()
dz = like.dz

# Open water masks
water_mask = xr.open_dataset(path_water_mask)["is_sea2d"]

# open dem
dem = xr.open_dataarray(path_dem)

# Calculate drain inputs
# drn depth
drn_depth = dem.where(water_mask.isnull())

# Conductance, take ~ 1 day for stability
# Divide resistance by cell area
conductance = abs(dx * dy) / resistance
drn_cond = xr.full_like(like_2d, 1.0) * conductance
drn_cond = drn_cond.where(water_mask.isnull())

# Save data
ds = xr.Dataset()
ds["drn_cond"] = drn_cond
ds["drn_depth"] = drn_depth
ds.to_netcdf(path_drn)

