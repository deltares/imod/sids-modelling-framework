import geopandas as gpd
import numpy as np
import xarray as xr

import imod

# Path to where the data is located
path_area1 = "data/1_external/wotje/DEM/ASTGTMV003_N09E170_dem_wotje.tif"
path_area2 = "data/1_external/wotje/DEM/gebco_wotje.tif"

# Opening data
dem1 = imod.rasterio.open(path_area1)
bathy = imod.rasterio.open(path_area2)

# Only selecting the dem where it is higher than 0., the bathymetrie will be added to the rest of the area
dem1 =dem1.where(dem1>0)

# Regrid the bathymetrie to the cellsize of the dem
bathy = imod.prepare.Regridder(method="mean").regrid(bathy,dem1)

# Devide both by 10, the numbers make more sense if they are a factor 10 smaller
dem1 = dem1 /10
bathy = bathy/10

# Combining DEM and bathymetrie
comb = dem1.combine_first(bathy)

# Saving the unaltered combined dataset to idfs
imod.idf.save("data/2_interim/wotje/bathymetrie_unaltered.idf", bathy)
imod.idf.save("data/2_interim/wotje/dem_unaltered.idf", dem1)

# Correcting the bathymetry to ensure everything around the island is below sealevel
masked =  bathy.where(dem1.isnull())
tmp = masked.where(masked>-0.8) -1.55
tmp = tmp.combine_first(masked)

# Combining the altered bathymetrie with the DEM
comb2 = dem1.combine_first(tmp)

# Saving the data to both IDF and TIF
imod.idf.save("data/2_interim/wotje/dem_corrected_complete.idf", comb2)
imod.rasterio.save("data/2_interim/wotje/dem_corrected_complete.tif", comb2)

# Selecting a part of the area, we don't need everything
xmin = 170.225
xmax = 170.250
ymin = 9.431
ymax =  9.472
comb3 = comb2.sel(y=slice(ymax, ymin)).sel(x=slice(xmin, xmax))

# Saving the data
imod.idf.save("data/2_interim/wotje/dem_corrected_select.idf", comb3)
imod.rasterio.save("data/2_interim/wotje/dem_corrected_select.tif", comb3)
