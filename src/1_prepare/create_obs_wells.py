import numpy as np
import pandas as pd
import xarray as xr
import geopandas as gpd
import math

import imod

def get_wells(x_value, y_value, numb_obs, max_depth, path_obs_wells_out):
    df_temp = pd.DataFrame()
    df_temp["x"] = x_value
    df_temp["y"] = y_value

    gdf = gpd.GeoDataFrame(
        df_temp, geometry=gpd.points_from_xy(df_temp["x"], df_temp["y"])
    )

    # Create island polygon
    island_poly = imod.prepare.polygonize(is_sea)
    island_poly = island_poly.loc[island_poly["value"] == 2.]
    gdf_mask = gdf.within(island_poly.loc[1, "geometry"])

    data = gdf.loc[gdf_mask].iloc[:numb_obs]

    df = pd.DataFrame()
    df["x"] = data["x"]
    df["y"] = data["y"]
    df.reset_index(inplace=True)
    df["id"] = (df.index.values  + 1).astype(str) 
    df["id"] = "obs_" + df["id"]

    # Get a random filterdepth between -0.5 and the entered maximum filter depth
    filt_depth = np.random.uniform(-0.5, max_depth, size=(numb_obs))
    df["filt_depth"] = filt_depth

    topish = like.ztop
    botish = like.zbot

    edges = np.empty(len(like.zbot)+1)
    edges[0] = topish.values[0]
    edges[1 : len(botish) + 1] = botish.values
    edges = np.flipud(edges)

    # Get layer in which filter is present
    filter_lay = (len(like.zbot)+1) - np.searchsorted(edges, df["filt_depth"])

    # Add it to main dataframe
    df["layer"] = filter_lay

    df = df[["x", "y", "id", "filt_depth", "layer"]]

    # save it
    df.to_csv(path_obs_wells_out)


# Snakemake paths
path_obs = snakemake.input.path_obs
path_water_mask = snakemake.input.path_water_mask
path_template = snakemake.input.path_template
path_obs_wells = snakemake.output.path_obs_wells

# params
params = snakemake.params
modelname = params["modelname"]
use_observation_wells = params["use_observation_wells"]
create_obs_wells = params["create_obs_wells"]
grid_style = params["grid_style"]
number_of_obs = params["number_of_obs"]
maximum_filt_depth = params["maximum_filt_depth"]

# Open data
like = xr.open_dataset(path_template)["template"]
is_sea = xr.open_dataset(path_water_mask)["is_sea2d"]
is_sea = is_sea.fillna(2.)

# Setting boundary settings
xmin = is_sea.x.min().values
xmax = is_sea.x.max().values
ymin = is_sea.y.min().values
ymax = is_sea.y.max().values

if use_observation_wells == True:
    if create_obs_wells == False:
        df_orig = pd.read_csv(path_obs)
        df = df_orig[["x", "y", "id", "filt_depth", "layer"]]

        if "conc" in df.columns:
            df["conc"] = df_orig["conc"]

        if "head" in df.columns:
            df["head"] = df_orig["head"]

        df.to_csv(path_obs_wells)

    if create_obs_wells == True:
        if grid_style == "uniform":
            # create a larger dataset and mask it with the 
            i_temp = number_of_obs * 5
            i_sqrt = math.ceil(math.sqrt(i_temp))
            list_x = np.linspace(xmin, xmax, num=i_sqrt)
            list_y = np.linspace(ymin, ymax, num=i_sqrt)

            dataxy = [(x,y) for x in list_x for y in list_y]

            df_temp = pd.DataFrame(dataxy, columns =["x", "y"])

            get_wells(df_temp["x"].values, df_temp["y"].values, number_of_obs, maximum_filt_depth, path_obs_wells)

        if grid_style == "random":
            i_temp = number_of_obs * 100
            x = np.random.randint(xmin, xmax, size=(i_temp))
            y = np.random.randint(ymin, ymax, size=(i_temp))

            get_wells(x, y, number_of_obs, maximum_filt_depth, path_obs_wells)
else:
    df = pd.DataFrame()
    df.to_csv(path_obs_wells)









